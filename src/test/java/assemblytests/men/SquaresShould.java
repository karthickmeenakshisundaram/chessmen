package assemblytests.men;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import men.Board;
import men.Square;
import men.Squares;
import men.exceptions.InvalidSquareException;
import men.pieces.Knight;

public class SquaresShould {

	@Test
	public void atleastOneSquareOccupied() {
		Board board=new Board(4,4);
		Squares squares = new Squares(Arrays.asList(new Square[]{board.getSquare("A1"), board.getSquare("A2"),board.getSquare("B2"),board.getSquare("C3")}));
		new Knight().place(board.getSquare("A1"));
		assertTrue("Atleast one square should have been occupied..But not the case!!",squares.atleastOneSquareOccupied());
	}
	
	@Test
	public void returnSquareByNotation() {
		Board board=new Board(4,4);
		Squares squares = new Squares(Arrays.asList(new Square[]{board.getSquare("A1"), board.getSquare("A2"),board.getSquare("B2"),board.getSquare("C3")}));
		assertEquals(squares.getSquare("C3"), board.getSquare("C3"));
	}
	
	@Test(expected=InvalidSquareException.class)
	public void returnInvalidSquareExceptionByNotation() {
		Board board=new Board(4,4);
		Squares squares = new Squares(Arrays.asList(new Square[]{board.getSquare("A1"), board.getSquare("A2"),board.getSquare("B2"),board.getSquare("C3")}));
		squares.getSquare("C1");
	}
	
	@Test
	public void returnFilesAvailable() {
		Board board=new Board(4,4);
		Squares squares = new Squares(Arrays.asList(new Square[]{board.getSquare("A1"), board.getSquare("A2"),board.getSquare("B2"),board.getSquare("C3")}));
		assertEquals(3,squares.filesAvailable().length);
		assertEquals(new Character('A'),squares.filesAvailable()[0]);
		assertEquals(new Character('B'),squares.filesAvailable()[1]);
		assertEquals(new Character('C'),squares.filesAvailable()[2]);
	}
	
	
}
