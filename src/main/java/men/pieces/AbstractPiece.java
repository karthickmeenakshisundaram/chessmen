package men.pieces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import men.Board;
import men.Piece;
import men.Square;
import men.exceptions.PieceNotPlacedAnyWhereYetException;

public abstract class AbstractPiece implements Piece{

	private static final Logger log = LoggerFactory.getLogger(AbstractPiece.class);
	private Square squareIOccupy;

	public Square whereAmI() throws PieceNotPlacedAnyWhereYetException{
		if(squareIOccupy==null){
			throw new PieceNotPlacedAnyWhereYetException(this);
		}
		return squareIOccupy;
	}
	
	public void setSquareIOccupy(Square squareIOccupy){
		this.squareIOccupy=squareIOccupy;
	}
	
	@Override
	public void resetSquareIOccupy() {
		this.squareIOccupy=null;
	}
	
	@Override
	public void place(Board board, int fileCoordinate, int rowCoordinate) {
		Square square = board.getSquare(fileCoordinate, rowCoordinate);
		place(square);
	}
	
	@Override
	public void place(Square square) {
		try{
			removeFromBoard();
			square.beOccupiedBy(this);
		}catch(PieceNotPlacedAnyWhereYetException e){
				square.beOccupiedBy(this);
				return;
		}
		
	}

	
	@Override
	public void place(Board board, String squareNotation) {
		Square square = board.getSquare(squareNotation);
		place(square);
	}
	
	@Override
	public void removeFromBoard() {
		try{
			this.whereAmI().vacate();
		}catch(PieceNotPlacedAnyWhereYetException e){
			//log.debug("this.piece {} not placed anywhere but remove from board was called.",this.getType());
		}
	}
	
	@Override
	public int compareTo(Piece piece) {
		return this.getType().compareTo(piece.getType());
	}

	@Override
	public String toString() {
		return getType().toString()+"["+(squareIOccupy==null?null:squareIOccupy.getNotation())+"]";
	}

	
	
	
}
