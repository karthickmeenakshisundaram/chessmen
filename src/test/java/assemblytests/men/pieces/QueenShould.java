package assemblytests.men.pieces;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import men.Piece;
import men.PieceType;
import men.pieces.Knight;
import men.pieces.Queen;

public class QueenShould {

	@Test
	public void returnPieceTypeAsQueen(){
		Piece queen=new Queen();
		queen.getType().equals(PieceType.Queen);
	}
	
	@Test
	public void returnFileWeightAs1(){
		Piece queen=new Queen();
		assertEquals(1,queen.fileWeight());
	}
	
}
