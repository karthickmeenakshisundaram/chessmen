package unittests.men;

import men.Doubler;
import org.junit.Assert;
import org.junit.Test;

public class DoublerTest {

    @Test
    public void testDouble(){
        Assert.assertEquals(new Integer(20),new Doubler().make(10));
    }

}
