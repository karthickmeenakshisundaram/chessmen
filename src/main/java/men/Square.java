package men;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import men.exceptions.IllegalVacateException;
import men.exceptions.NotSupportedException;
import men.exceptions.SquareAlreadyOccupiedException;

public class Square implements Comparable<Square>{

	private final int i;
	private final int j;
	private final char file; 
	private String notation;
	private Piece occupyingPiece;
	

	private static final Map<Integer,Character> iToA=new HashMap<Integer,Character>();
	private static final Map<Character,Integer> aToI=new HashMap<Character,Integer>();
	
	static{
		int alphabetsAscii=65;
		for(int i=1;i<=26;i++){
			iToA.put(new Integer(i), (char)alphabetsAscii);
			aToI.put((char)alphabetsAscii,new Integer(i));
			alphabetsAscii++;
		}
	}
	
	public Square(int i, int j) throws NotSupportedException {
		if(i>26){
			throw new NotSupportedException("Coordinates :["+i+" "+j + "] not supported. max values for i is 26 only");
		}
		this.i=i;
		this.j=j;
		this.file=iToA.get(i);
		this.notation=""+iToA.get(i)+j;
	}
	
	public Square(String notation) throws NotSupportedException {
		this.notation=notation;
		if(StringUtils.isEmpty(notation)){
			throw new NotSupportedException("invalid notation "+notation);
		}
		try {
			file = notation.charAt(0);
			Integer row=0;
			if(!aToI.containsKey(file)){
				throw new NotSupportedException("File "+file+ " is not supported.");
			}
			try{
				row = Integer.valueOf(notation.substring(1));
				this.i=aToI.get(file);
				this.j=row;
			}catch(NumberFormatException nfe){
				throw new NotSupportedException("row "+notation.substring(1)+ " is not supported.");
			}
		} catch (NotSupportedException | ArrayIndexOutOfBoundsException e) {
			throw new NotSupportedException("invalid notation "+notation,e);
		}
	}

	public String getNotation() {
		return notation;
	}

	public int getJ() {
		return j;
	}

	public int getI() {
		return i;
	}
	
	

	public char getFile() {
		return file;
	}

	public boolean isOccupied() {
		return this.occupyingPiece!=null;
	}

	public void beOccupiedBy(Piece piece) throws SquareAlreadyOccupiedException{
		if(this.occupyingPiece!=null)
			throw new SquareAlreadyOccupiedException(this);
		this.occupyingPiece=piece;
		occupyingPiece.setSquareIOccupy(this);
	}

	public Piece getOccupiedBy() {
		return occupyingPiece;
	}

	public void vacate() throws IllegalVacateException {
		if(this.occupyingPiece==null)
			throw new IllegalVacateException(this);
		this.occupyingPiece.resetSquareIOccupy();
		this.occupyingPiece=null;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + i;
		result = prime * result + j;
		result = prime * result + ((notation == null) ? 0 : notation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Square other = (Square) obj;
		if (i != other.i)
			return false;
		if (j != other.j)
			return false;
		if (notation == null) {
			if (other.notation != null)
				return false;
		} else if (!notation.equals(other.notation))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Square [notation=" + notation + ", occupyingPiece=" + occupyingPiece + "]";
	}

	@Override
	public int compareTo(Square o) {
		return this.getNotation().compareTo(o.getNotation());
	}

	public static char getFileForI(int i){
		return iToA.get(i);
	}
	

}
