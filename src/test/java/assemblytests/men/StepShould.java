package assemblytests.men;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;

import org.junit.Test;

import ch.qos.logback.core.net.SyslogOutputStream;
import men.Arrangement;
import men.Board;
import men.Piece;
import men.Pieces;
import men.Step;
import men.pieces.Bishop;
import men.pieces.Knight;
import men.pieces.Queen;
import men.pieces.Rook;

public class StepShould {

/*	@Test
	public void testName() throws Exception {
		Board board=new Board(4,4);
		Rook rook1 = new Rook();
		Rook rook2 = new Rook();
		Rook rook3 = new Rook();
		Rook rook4 = new Rook();
		Pieces pieces = new Pieces(Arrays.asList(new Piece[] { rook1, rook2, rook3, rook4 }));
		Step step=new Step(board, pieces);
		step.startCalculating();
	}*/
	@Test
	public void returnAllPositionsBoard_4_4_ForASingleRook() {
		Board board=new Board(4,4);
		Rook rook1 = new Rook();
		Pieces pieces = new Pieces(Arrays.asList(new Piece[] { rook1}));
		Step step=new Step(board, pieces);
		step.startCalculating();
		Set<Arrangement> arrangements = board.getArrangements();
		Arrangement[] expectedArrangements={
				Arrangement.from("RA1"),
				Arrangement.from("RA2"),
				Arrangement.from("RA3"),
				Arrangement.from("RA4"),
				Arrangement.from("RB1"),
				Arrangement.from("RB2"),
				Arrangement.from("RB3"),
				Arrangement.from("RB4"),
				Arrangement.from("RC1"),
				Arrangement.from("RC2"),
				Arrangement.from("RC3"),
				Arrangement.from("RC4"),
				Arrangement.from("RD1"),
				Arrangement.from("RD2"),
				Arrangement.from("RD3"),
				Arrangement.from("RD4")
		};
		for (Arrangement expectedArrangement: expectedArrangements) {
			assertTrue("Arrangements doesn't contain "+expectedArrangement,arrangements.contains(expectedArrangement));
		}
		assertEquals(16,arrangements.size());
	}
	
	@Test
	public void returnAllPositionsBoard_2_2_ForTwoRooks(){
		Board board=new Board(2,2);
		Rook rook1 = new Rook();
		Rook rook2 = new Rook();
		Pieces pieces = new Pieces(Arrays.asList(new Piece[] { rook1, rook2}));
		Step step=new Step(board, pieces);
		step.startCalculating();
		Set<Arrangement> arrangements = board.getArrangements();
		assertEquals(2,arrangements.size());
		Arrangement[] expectedArrangements={
				Arrangement.from("RA1 RB2"),
				Arrangement.from("RA2 RB1")
		};
		for (Arrangement expectedArrangement: expectedArrangements) {
			assertTrue("Arrangements doesn't contain "+expectedArrangement,arrangements.contains(expectedArrangement));
		}
	}
	
	@Test
	public void returnAllPositionsBoard_3_3_ForThreeRooks(){
		Board board=new Board(3,3);
		Rook rook1 = new Rook();
		Rook rook2 = new Rook();
		Rook rook3 = new Rook();
		Pieces pieces = new Pieces(Arrays.asList(new Piece[] { rook1, rook2, rook3}));
		Step step=new Step(board, pieces);
		step.startCalculating();
		Set<Arrangement> arrangements = board.getArrangements();
		assertEquals(6,arrangements.size());
		Arrangement[] expectedArrangements={
		Arrangement.from("RA1 RB2 RC3"),
		Arrangement.from("RA1 RB3 RC2"),
		Arrangement.from("RA2 RB1 RC3"),
		Arrangement.from("RA2 RB3 RC1"),
		Arrangement.from("RA3 RB1 RC2"),
		Arrangement.from("RA3 RB2 RC1")};
		for (Arrangement expectedArrangement: expectedArrangements) {
			assertTrue("Arrangements doesn't contain "+expectedArrangement,arrangements.contains(expectedArrangement));
		}
	}
	
	@Test
	public void returnAllPositionsBoard_4_4_ForThreeRooksAndAKnight(){
		Board board=new Board(4,4);
		Rook rook1 = new Rook();
		Rook rook2 = new Rook();
		Rook rook3 = new Rook();
		Knight knight1 = new Knight();
		Pieces pieces = new Pieces(Arrays.asList(new Piece[] { rook1, rook2, rook3, knight1}));
		Step step=new Step(board, pieces);
		step.startCalculating();
		Set<Arrangement> arrangements = board.getArrangements();
		assertEquals(36,arrangements.size());
		Arrangement[] expectedArrangements={
		Arrangement.from("HA1 RB2 RC3 RD4"),
		Arrangement.from("HA1 RB2 RC4 RD3"),
		Arrangement.from("HA1 RB4 RC3 RD2"),
		Arrangement.from("HA2 RB1 RC4 RD3"),
		Arrangement.from("HA2 RB3 RC4 RD1"),
		Arrangement.from("HA3 RB2 RC1 RD4"),
		Arrangement.from("HA3 RB4 RC1 RD2"),
		Arrangement.from("HA4 RB1 RC2 RD3"),
		Arrangement.from("HA4 RB3 RC1 RD2"),
		Arrangement.from("HA4 RB3 RC2 RD1"),
		Arrangement.from("RA1 HB2 RC3 RD4"),
		Arrangement.from("RA1 HB4 RC3 RD2"),
		Arrangement.from("RA1 RB2 HC3 RD4"),
		Arrangement.from("RA1 RB2 RC3 HD4"),
		Arrangement.from("RA1 RB4 HC3 RD2"),
		Arrangement.from("RA1 RB4 RC3 HD2"),
		Arrangement.from("RA2 HB1 RC4 RD3"),
		Arrangement.from("RA2 HB3 RC4 RD1"),
		Arrangement.from("RA2 RB1 HC4 RD3"),
		Arrangement.from("RA2 RB1 RC3 HD4"),
		Arrangement.from("RA2 RB1 RC4 HD3"),
		Arrangement.from("RA2 RB3 HC4 RD1"),
		Arrangement.from("RA2 RB3 RC4 HD1"),
		Arrangement.from("RA3 HB2 RC1 RD4"),
		Arrangement.from("RA3 HB4 RC1 RD2"),
		Arrangement.from("RA3 RB2 HC1 RD4"),
		Arrangement.from("RA3 RB2 RC1 HD4"),
		Arrangement.from("RA3 RB4 HC1 RD2"),
		Arrangement.from("RA3 RB4 RC1 HD2"),
		Arrangement.from("RA3 RB4 RC2 HD1"),
		Arrangement.from("RA4 HB1 RC2 RD3"),
		Arrangement.from("RA4 HB3 RC2 RD1"),
		Arrangement.from("RA4 RB1 HC2 RD3"),
		Arrangement.from("RA4 RB1 RC2 HD3"),
		Arrangement.from("RA4 RB3 HC2 RD1"),
		Arrangement.from("RA4 RB3 RC2 HD1")
		};
		for (Arrangement expectedArrangement: expectedArrangements) {
			assertTrue("Arrangements doesn't contain "+expectedArrangement,arrangements.contains(expectedArrangement));
		}
	}
	
	@Test
	public void returnAllPositions_Board_8_8_ForEightQueens(){
		Board board=new Board(8,8);
		Pieces pieces = new Pieces(Arrays.asList(new Piece[] { new Queen(), new Queen(), new Queen(), new Queen(),new Queen(),new Queen(),new Queen(),new Queen()}));
		Step step=new Step(board, pieces);
		step.startCalculating();
		Set<Arrangement> arrangements = board.getArrangements();
		assertEquals(92,arrangements.size());
		Arrangement[] expectedArrangements={Arrangement.from("QA1 QB5 QC8 QD6 QE3 QF7 QG2 QH4"),
		Arrangement.from("QA1 QB6 QC8 QD3 QE7 QF4 QG2 QH5"),
		Arrangement.from("QA1 QB7 QC4 QD6 QE8 QF2 QG5 QH3"),
		Arrangement.from("QA1 QB7 QC5 QD8 QE2 QF4 QG6 QH3"),
		Arrangement.from("QA2 QB4 QC6 QD8 QE3 QF1 QG7 QH5"),
		Arrangement.from("QA2 QB5 QC7 QD1 QE3 QF8 QG6 QH4"),
		Arrangement.from("QA2 QB5 QC7 QD4 QE1 QF8 QG6 QH3"),
		Arrangement.from("QA2 QB6 QC1 QD7 QE4 QF8 QG3 QH5"),
		Arrangement.from("QA2 QB6 QC8 QD3 QE1 QF4 QG7 QH5"),
		Arrangement.from("QA2 QB7 QC3 QD6 QE8 QF5 QG1 QH4"),
		Arrangement.from("QA2 QB7 QC5 QD8 QE1 QF4 QG6 QH3"),
		Arrangement.from("QA2 QB8 QC6 QD1 QE3 QF5 QG7 QH4"),
		Arrangement.from("QA3 QB1 QC7 QD5 QE8 QF2 QG4 QH6"),
		Arrangement.from("QA3 QB5 QC2 QD8 QE1 QF7 QG4 QH6"),
		Arrangement.from("QA3 QB5 QC2 QD8 QE6 QF4 QG7 QH1"),
		Arrangement.from("QA3 QB5 QC7 QD1 QE4 QF2 QG8 QH6"),
		Arrangement.from("QA3 QB5 QC8 QD4 QE1 QF7 QG2 QH6"),
		Arrangement.from("QA3 QB6 QC2 QD5 QE8 QF1 QG7 QH4"),
		Arrangement.from("QA3 QB6 QC2 QD7 QE1 QF4 QG8 QH5"),
		Arrangement.from("QA3 QB6 QC2 QD7 QE5 QF1 QG8 QH4"),
		Arrangement.from("QA3 QB6 QC4 QD1 QE8 QF5 QG7 QH2"),
		Arrangement.from("QA3 QB6 QC4 QD2 QE8 QF5 QG7 QH1"),
		Arrangement.from("QA3 QB6 QC8 QD1 QE4 QF7 QG5 QH2"),
		Arrangement.from("QA3 QB6 QC8 QD1 QE5 QF7 QG2 QH4"),
		Arrangement.from("QA3 QB6 QC8 QD2 QE4 QF1 QG7 QH5"),
		Arrangement.from("QA3 QB7 QC2 QD8 QE5 QF1 QG4 QH6"),
		Arrangement.from("QA3 QB7 QC2 QD8 QE6 QF4 QG1 QH5"),
		Arrangement.from("QA3 QB8 QC4 QD7 QE1 QF6 QG2 QH5"),
		Arrangement.from("QA4 QB1 QC5 QD8 QE2 QF7 QG3 QH6"),
		Arrangement.from("QA4 QB1 QC5 QD8 QE6 QF3 QG7 QH2"),
		Arrangement.from("QA4 QB2 QC5 QD8 QE6 QF1 QG3 QH7"),
		Arrangement.from("QA4 QB2 QC7 QD3 QE6 QF8 QG1 QH5"),
		Arrangement.from("QA4 QB2 QC7 QD3 QE6 QF8 QG5 QH1"),
		Arrangement.from("QA4 QB2 QC7 QD5 QE1 QF8 QG6 QH3"),
		Arrangement.from("QA4 QB2 QC8 QD5 QE7 QF1 QG3 QH6"),
		Arrangement.from("QA4 QB2 QC8 QD6 QE1 QF3 QG5 QH7"),
		Arrangement.from("QA4 QB6 QC1 QD5 QE2 QF8 QG3 QH7"),
		Arrangement.from("QA4 QB6 QC8 QD2 QE7 QF1 QG3 QH5"),
		Arrangement.from("QA4 QB6 QC8 QD3 QE1 QF7 QG5 QH2"),
		Arrangement.from("QA4 QB7 QC1 QD8 QE5 QF2 QG6 QH3"),
		Arrangement.from("QA4 QB7 QC3 QD8 QE2 QF5 QG1 QH6"),
		Arrangement.from("QA4 QB7 QC5 QD2 QE6 QF1 QG3 QH8"),
		Arrangement.from("QA4 QB7 QC5 QD3 QE1 QF6 QG8 QH2"),
		Arrangement.from("QA4 QB8 QC1 QD3 QE6 QF2 QG7 QH5"),
		Arrangement.from("QA4 QB8 QC1 QD5 QE7 QF2 QG6 QH3"),
		Arrangement.from("QA4 QB8 QC5 QD3 QE1 QF7 QG2 QH6"),
		Arrangement.from("QA5 QB1 QC4 QD6 QE8 QF2 QG7 QH3"),
		Arrangement.from("QA5 QB1 QC8 QD4 QE2 QF7 QG3 QH6"),
		Arrangement.from("QA5 QB1 QC8 QD6 QE3 QF7 QG2 QH4"),
		Arrangement.from("QA5 QB2 QC4 QD6 QE8 QF3 QG1 QH7"),
		Arrangement.from("QA5 QB2 QC4 QD7 QE3 QF8 QG6 QH1"),
		Arrangement.from("QA5 QB2 QC6 QD1 QE7 QF4 QG8 QH3"),
		Arrangement.from("QA5 QB2 QC8 QD1 QE4 QF7 QG3 QH6"),
		Arrangement.from("QA5 QB3 QC1 QD6 QE8 QF2 QG4 QH7"),
		Arrangement.from("QA5 QB3 QC1 QD7 QE2 QF8 QG6 QH4"),
		Arrangement.from("QA5 QB3 QC8 QD4 QE7 QF1 QG6 QH2"),
		Arrangement.from("QA5 QB7 QC1 QD3 QE8 QF6 QG4 QH2"),
		Arrangement.from("QA5 QB7 QC1 QD4 QE2 QF8 QG6 QH3"),
		Arrangement.from("QA5 QB7 QC2 QD4 QE8 QF1 QG3 QH6"),
		Arrangement.from("QA5 QB7 QC2 QD6 QE3 QF1 QG4 QH8"),
		Arrangement.from("QA5 QB7 QC2 QD6 QE3 QF1 QG8 QH4"),
		Arrangement.from("QA5 QB7 QC4 QD1 QE3 QF8 QG6 QH2"),
		Arrangement.from("QA5 QB8 QC4 QD1 QE3 QF6 QG2 QH7"),
		Arrangement.from("QA5 QB8 QC4 QD1 QE7 QF2 QG6 QH3"),
		Arrangement.from("QA6 QB1 QC5 QD2 QE8 QF3 QG7 QH4"),
		Arrangement.from("QA6 QB2 QC7 QD1 QE3 QF5 QG8 QH4"),
		Arrangement.from("QA6 QB2 QC7 QD1 QE4 QF8 QG5 QH3"),
		Arrangement.from("QA6 QB3 QC1 QD7 QE5 QF8 QG2 QH4"),
		Arrangement.from("QA6 QB3 QC1 QD8 QE4 QF2 QG7 QH5"),
		Arrangement.from("QA6 QB3 QC1 QD8 QE5 QF2 QG4 QH7"),
		Arrangement.from("QA6 QB3 QC5 QD7 QE1 QF4 QG2 QH8"),
		Arrangement.from("QA6 QB3 QC5 QD8 QE1 QF4 QG2 QH7"),
		Arrangement.from("QA6 QB3 QC7 QD2 QE4 QF8 QG1 QH5"),
		Arrangement.from("QA6 QB3 QC7 QD2 QE8 QF5 QG1 QH4"),
		Arrangement.from("QA6 QB3 QC7 QD4 QE1 QF8 QG2 QH5"),
		Arrangement.from("QA6 QB4 QC1 QD5 QE8 QF2 QG7 QH3"),
		Arrangement.from("QA6 QB4 QC2 QD8 QE5 QF7 QG1 QH3"),
		Arrangement.from("QA6 QB4 QC7 QD1 QE3 QF5 QG2 QH8"),
		Arrangement.from("QA6 QB4 QC7 QD1 QE8 QF2 QG5 QH3"),
		Arrangement.from("QA6 QB8 QC2 QD4 QE1 QF7 QG5 QH3"),
		Arrangement.from("QA7 QB1 QC3 QD8 QE6 QF4 QG2 QH5"),
		Arrangement.from("QA7 QB2 QC4 QD1 QE8 QF5 QG3 QH6"),
		Arrangement.from("QA7 QB2 QC6 QD3 QE1 QF4 QG8 QH5"),
		Arrangement.from("QA7 QB3 QC1 QD6 QE8 QF5 QG2 QH4"),
		Arrangement.from("QA7 QB3 QC8 QD2 QE5 QF1 QG6 QH4"),
		Arrangement.from("QA7 QB4 QC2 QD5 QE8 QF1 QG3 QH6"),
		Arrangement.from("QA7 QB4 QC2 QD8 QE6 QF1 QG3 QH5"),
		Arrangement.from("QA7 QB5 QC3 QD1 QE6 QF8 QG2 QH4"),
		Arrangement.from("QA8 QB2 QC4 QD1 QE7 QF5 QG3 QH6"),
		Arrangement.from("QA8 QB2 QC5 QD3 QE1 QF7 QG4 QH6"),
		Arrangement.from("QA8 QB3 QC1 QD6 QE2 QF5 QG7 QH4"),
		Arrangement.from("QA8 QB4 QC1 QD3 QE6 QF2 QG7 QH5")};
		for (Arrangement expectedArrangement: expectedArrangements) {
			assertTrue("Arrangements doesn't contain "+expectedArrangement,arrangements.contains(expectedArrangement));
		}
	}
	
	/*@Test
	public void returnAllPositions_Board_8_8_ForEightRooks(){
		Board board=new Board(8,8);
		Pieces pieces = new Pieces(Arrays.asList(new Piece[] { new Rook(), new Rook(), new Rook(), new Rook(),new Rook(),new Rook(),new Rook(),new Rook()}));
		Step step=new Step(board, pieces);
		step.startCalculating();
		Set<Arrangement> arrangements = board.getArrangements();
		assertEquals(8*7*6*5*4*3*2*1,arrangements.size());
	}*/
	
	@Test
	public void returnNoPositions_Board_4_4_ForSevenRooksAndAKnight(){
		Board board=new Board(4,4);
		Pieces pieces = new Pieces(Arrays.asList(new Piece[] { new Rook(), new Rook(), new Rook(), new Rook(),new Rook(),new Rook(),new Rook(),new Knight()}));
		Step step=new Step(board, pieces);
		step.startCalculating();
		Set<Arrangement> arrangements = board.getArrangements();
		assertEquals(0,arrangements.size());
	}
	@Test
	public void returnAllPositions_Board_4_4_ForThreeRooksAndAKnight(){
		Board board=new Board(4,4);
		Pieces pieces = new Pieces(Arrays.asList(new Piece[] { new Rook(), new Rook(), new Rook(),new Knight()}));
		Step step=new Step(board, pieces);
		step.startCalculating();
		Set<Arrangement> arrangements = board.getArrangements();
		Arrangement[] expectedArrangements={
		Arrangement.from("HA1 RB2 RC3 RD4"),
		Arrangement.from("HA1 RB2 RC4 RD3"),
		Arrangement.from("HA1 RB4 RC3 RD2"),
		Arrangement.from("RA1 HB2 RC3 RD4"),
		Arrangement.from("RA1 RB2 HC3 RD4"),
		Arrangement.from("RA1 RB2 RC3 HD4"),
		Arrangement.from("RA1 HB4 RC3 RD2"),
		Arrangement.from("RA1 RB4 HC3 RD2"),
		Arrangement.from("RA1 RB4 RC3 HD2"),
		Arrangement.from("HA2 RB1 RC4 RD3"),
		Arrangement.from("HA2 RB3 RC4 RD1"),
		Arrangement.from("RA2 HB1 RC4 RD3"),
		Arrangement.from("RA2 RB1 RC3 HD4"),
		Arrangement.from("RA2 RB1 HC4 RD3"),
		Arrangement.from("RA2 RB1 RC4 HD3"),
		Arrangement.from("RA2 HB3 RC4 RD1"),
		Arrangement.from("RA2 RB3 HC4 RD1"),
		Arrangement.from("RA2 RB3 RC4 HD1"),
		Arrangement.from("HA3 RB2 RC1 RD4"),
		Arrangement.from("HA3 RB4 RC1 RD2"),
		Arrangement.from("RA3 HB2 RC1 RD4"),
		Arrangement.from("RA3 RB2 HC1 RD4"),
		Arrangement.from("RA3 RB2 RC1 HD4"),
		Arrangement.from("RA3 HB4 RC1 RD2"),
		Arrangement.from("RA3 RB4 HC1 RD2"),
		Arrangement.from("RA3 RB4 RC1 HD2"),
		Arrangement.from("RA3 RB4 RC2 HD1"),
		Arrangement.from("HA4 RB1 RC2 RD3"),
		Arrangement.from("HA4 RB3 RC1 RD2"),
		Arrangement.from("HA4 RB3 RC2 RD1"),
		Arrangement.from("RA4 HB1 RC2 RD3"),
		Arrangement.from("RA4 RB1 HC2 RD3"),
		Arrangement.from("RA4 RB1 RC2 HD3"),
		Arrangement.from("RA4 HB3 RC2 RD1"),
		Arrangement.from("RA4 RB3 HC2 RD1"),
		Arrangement.from("RA4 RB3 RC2 HD1")
		};
		assertEquals(36,arrangements.size());
		for (Arrangement expectedArrangement: expectedArrangements) {
			assertTrue("Arrangements doesn't contain "+expectedArrangement,arrangements.contains(expectedArrangement));
		}
	}
	
	@Test
	public void returnAllPositions_Board_2_2_ForFourKnight(){
		Board board=new Board(2,2);
		Pieces pieces = new Pieces(Arrays.asList(new Piece[] { new Knight(), new Knight(), new Knight(),new Knight()}));
		Step step=new Step(board, pieces);
		step.startCalculating();
		Set<Arrangement> arrangements = board.getArrangements();
		assertEquals(1,arrangements.size());
		Arrangement[] expectedArrangements={Arrangement.from("HA1 HA2 HB1 HB2")};
		for (Arrangement expectedArrangement: expectedArrangements) {
			assertTrue("Arrangements doesn't contain "+expectedArrangement,arrangements.contains(expectedArrangement));
		}
	}
	
	@Test
	public void returnAllPositions_Board_2_2_ForTwoKnightsAndABishop(){
		Board board=new Board(2,2);
		Pieces pieces = new Pieces(Arrays.asList(new Piece[] { new Knight(), new Knight(), new Bishop()}));
		Step step=new Step(board, pieces);
		step.startCalculating();
		Set<Arrangement> arrangements = board.getArrangements();
		assertEquals(4,arrangements.size());
		Arrangement[] expectedArrangements={Arrangement.from("BA1 HA2 HB1"),
				Arrangement.from("HA1 BA2 HB2"),
				Arrangement.from("HA1 BB1 HB2"),
				Arrangement.from("HA2 HB1 BB2")};
		for (Arrangement expectedArrangement: expectedArrangements) {
			assertTrue("Arrangements doesn't contain "+expectedArrangement,arrangements.contains(expectedArrangement));
		}
	}
	
	@Test
	public void returnAllPositions_Board_4_4_ForTwoKnightsAndABishopAndTwoRooks(){
		Board board=new Board(4,4);
		Pieces pieces = new Pieces(Arrays.asList(new Piece[] { new Knight(), new Knight(), new Bishop(), new Rook(), new Rook()}));
		Step step=new Step(board, pieces);
		step.startCalculating();
		Set<Arrangement> arrangements = board.getArrangements();
		assertEquals(68,arrangements.size());
		Arrangement[] expectedArrangements={
				Arrangement.from("BA1 HA2 HB1 RC4 RD3"),
				Arrangement.from("BA1 HA2 RB3 RC4 HD1"),
				Arrangement.from("BA1 HA3 RB4 HC1 RD2"),
				Arrangement.from("BA1 HA4 HB1 RC2 RD3"),
				Arrangement.from("BA1 HA4 RB3 RC2 HD1"),
				Arrangement.from("HA1 BA2 HB2 RC3 RD4"),
				Arrangement.from("HA1 BA2 RB4 RC3 HD2"),
				Arrangement.from("HA1 BA4 RB2 RC3 HD4"),
				Arrangement.from("HA1 BA4 HB4 RC3 RD2"),
				Arrangement.from("HA1 BB1 HB2 RC3 RD4"),
				Arrangement.from("HA1 BB1 HB4 RC3 RD2"),
				Arrangement.from("HA1 RB2 RC3 BD1 HD4"),
				Arrangement.from("HA1 RB4 RC3 BD1 HD2"),
				Arrangement.from("RA1 HB2 BB3 HC3 RD4"),
				Arrangement.from("RA1 HB2 BC2 HC3 RD4"),
				Arrangement.from("RA1 RB2 HC3 BC4 HD4"),
				Arrangement.from("RA1 RB2 HC3 BD3 HD4"),
				Arrangement.from("RA1 BB3 HB4 HC3 RD2"),
				Arrangement.from("RA1 HB4 HC3 BC4 RD2"),
				Arrangement.from("RA1 RB4 BC2 HC3 HD2"),
				Arrangement.from("RA1 RB4 HC3 HD2 BD3"),
				Arrangement.from("BA2 HA3 HB2 RC1 RD4"),
				Arrangement.from("BA2 HA3 RB4 RC1 HD2"),
				Arrangement.from("HA2 BA3 RB1 RC4 HD3"),
				Arrangement.from("HA2 BA3 HB3 RC4 RD1"),
				Arrangement.from("HA2 BA4 RB1 HC4 RD3"),
				Arrangement.from("HA2 HB1 BB2 RC4 RD3"),
				Arrangement.from("HA2 RB1 RC4 BD2 HD3"),
				Arrangement.from("HA2 BB2 HB3 RC4 RD1"),
				Arrangement.from("HA2 RB3 RC4 HD1 BD2"),
				Arrangement.from("RA2 HB1 BB4 HC4 RD3"),
				Arrangement.from("RA2 HB1 BC1 HC4 RD3"),
				Arrangement.from("RA2 HB1 RC4 BD1 HD3"),
				Arrangement.from("RA2 RB1 BC3 HC4 HD3"),
				Arrangement.from("RA2 RB1 HC4 HD3 BD4"),
				Arrangement.from("RA2 HB3 BB4 HC4 RD1"),
				Arrangement.from("RA2 HB3 BC3 HC4 RD1"),
				Arrangement.from("RA2 RB3 BC1 HC4 HD1"),
				Arrangement.from("RA2 RB3 HC4 HD1 BD4"),
				Arrangement.from("BA3 HA4 RB1 RC2 HD3"),
				Arrangement.from("BA3 HA4 HB3 RC2 RD1"),
				Arrangement.from("HA3 BA4 RB2 RC1 HD4"),
				Arrangement.from("HA3 BA4 HB4 RC1 RD2"),
				Arrangement.from("HA3 HB2 BB3 RC1 RD4"),
				Arrangement.from("HA3 RB2 RC1 BD3 HD4"),
				Arrangement.from("HA3 BB3 HB4 RC1 RD2"),
				Arrangement.from("HA3 RB4 RC1 HD2 BD3"),
				Arrangement.from("RA3 BB1 HB2 HC1 RD4"),
				Arrangement.from("RA3 BB1 HB4 HC1 RD2"),
				Arrangement.from("RA3 HB2 HC1 BC2 RD4"),
				Arrangement.from("RA3 RB2 HC1 BC4 HD4"),
				Arrangement.from("RA3 RB2 HC1 BD1 HD4"),
				Arrangement.from("RA3 HB4 HC1 BC4 RD2"),
				Arrangement.from("RA3 HB4 RC1 HD2 BD4"),
				Arrangement.from("RA3 RB4 HC1 BC2 HD2"),
				Arrangement.from("RA3 RB4 HC1 BD1 HD2"),
				Arrangement.from("HA4 HB1 BB4 RC2 RD3"),
				Arrangement.from("HA4 RB1 RC2 HD3 BD4"),
				Arrangement.from("HA4 HB3 BB4 RC2 RD1"),
				Arrangement.from("HA4 RB3 RC2 HD1 BD4"),
				Arrangement.from("RA4 HB1 BB2 HC2 RD3"),
				Arrangement.from("RA4 HB1 BC1 HC2 RD3"),
				Arrangement.from("RA4 RB1 HC2 BC3 HD3"),
				Arrangement.from("RA4 RB1 HC2 BD2 HD3"),
				Arrangement.from("RA4 BB2 HB3 HC2 RD1"),
				Arrangement.from("RA4 HB3 HC2 BC3 RD1"),
				Arrangement.from("RA4 RB3 BC1 HC2 HD1"),
				Arrangement.from("RA4 RB3 HC2 HD1 BD2")
				};
		for (Arrangement expectedArrangement: expectedArrangements) {
			assertTrue("Arrangements doesn't contain "+expectedArrangement,arrangements.contains(expectedArrangement));
		}
	}
}
