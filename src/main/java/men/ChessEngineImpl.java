package men;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.util.StatusPrinter;
import men.exceptions.CantCreatePieceException;
import men.exceptions.IllegalBoardException;
import men.exceptions.InvalidInputException;
import men.exceptions.NotSupportedException;
import men.pieces.Bishop;
import men.pieces.King;
import men.pieces.Knight;
import men.pieces.Queen;
import men.pieces.Rook;

public class ChessEngineImpl implements ChessEngine {

	private static final Logger log = LoggerFactory.getLogger(ChessEngineImpl.class);
	
	public static class BoardBuilder{
		private int files;
		private int rows;
		
		private Predicate<Integer> integerGreaterThanZeroPredicate;
		private Scanner sc;
		private List<Predicate<Integer>> integerNotMoreThan26AndGreaterThan02;
		
		public BoardBuilder(Scanner sc, Predicate<Integer> integerGreaterThanZeroPredicate, List<Predicate<Integer>> integerNotMoreThan26AndGreaterThan02){
			this.sc=sc;
			this.integerGreaterThanZeroPredicate=integerGreaterThanZeroPredicate;
			this.integerNotMoreThan26AndGreaterThan02=integerNotMoreThan26AndGreaterThan02;
			withFilesAndRows(getInt(sc, "Enter the number of files in the board.", integerNotMoreThan26AndGreaterThan02),
					getInt(sc, "Enter the number of rows in the board." , Collections.singletonList(integerGreaterThanZeroPredicate)));
		}
		
		public BoardBuilder withFilesAndRows(int files, int rows){
			this.files=files;
			this.rows=rows;
			return this;
		}
		public Board build(){
			try{
				return new Board(files, rows);
			}catch(IllegalBoardException | NotSupportedException e){
				System.out.println(e.getMessage() + " Try Again!");
				return new BoardBuilder(sc, integerGreaterThanZeroPredicate, integerNotMoreThan26AndGreaterThan02).build();
			}
		}
	}
	
	public static class PiecesBuilder {
		private int rooks;
		private int queens;
		private int knights;
		private int bishops;
		private int kings;
		
		private List<Piece> piecesAsList=new ArrayList<Piece>();
		private Predicate<Integer> integerNotNegativePredicate;
		private Scanner sc;
		
		public PiecesBuilder(Scanner sc, Predicate<Integer> integerNotNegativePredicate) {
			this.sc=sc;
			this.integerNotNegativePredicate=integerNotNegativePredicate;
			withNoOfQueens(getInt(sc, "Enter the number of Queens to place",Collections.singletonList(integerNotNegativePredicate)));
			withNoOfKings(
					getInt(sc, "Enter the number of kings to place",Collections.singletonList(integerNotNegativePredicate)));
			withNoOfRooks(
					getInt(sc, "Enter the number of Rooks to place",Collections.singletonList(integerNotNegativePredicate)));
			withNoOfBishops(getInt(sc, "Enter the number of Bishops to place",Collections.singletonList(integerNotNegativePredicate)));
			withNoOfKnights(
					getInt(sc, "Enter the number of Knights to place",Collections.singletonList(integerNotNegativePredicate)));
		}
		
		public void withNoOfKings(int kings) {
			this.kings=kings;
		}

		public PiecesBuilder(){
			
		}
		
		public PiecesBuilder withNoOfRooks(int rooks){
			this.rooks=rooks;
			return this;
		}
		public PiecesBuilder withNoOfQueens(int queens){
			this.queens=queens;
			return this;
		}
		public PiecesBuilder withNoOfKnights(int knights){
			this.knights=knights;
			return this;
		}
		public PiecesBuilder withNoOfBishops(int bishops){
			this.bishops=bishops;
			return this;
		}
		public Pieces build(){
			if(rooks+queens+knights+bishops+kings>0){
				addPieces(queens,Queen.class);
				addPieces(rooks, Rook.class);
				addPieces(bishops,Bishop.class);
				addPieces(knights, Knight.class);
				addPieces(kings, King.class);
			}else{
				System.out.println("Atleast one of NoOfRooks, NoOfQueens, NoOfBishops, NoOfKnights, NoOfKings should be non zero. Try again!");
				return new PiecesBuilder(sc,integerNotNegativePredicate).build();
			}
			return new Pieces(piecesAsList);
		}
		private void addPieces(int queens, Class<? extends Piece> class1) {
			for(int i=0;i<queens;i++){
				try {
					piecesAsList.add(class1.newInstance());
				} catch (InstantiationException | IllegalAccessException e) {
					throw new CantCreatePieceException(class1);
				}
			}
		}
	}

	private Board board;
	private Pieces pieces;
	private Instant startOfCalculations;
	private Instant endOfCalculations;
	
	/*
	 * InputMismatchException - if the next token does not match the Integer
	 * regular expression, or is out of range NoSuchElementException - if input
	 * is exhausted IllegalStateException - if this scanner is closed
	 */

	public static void main(String[] args) {
		System.out.println("Welcome to the Chess Board Positions App");
		if(System.getProperty("logback.statusListenerClass")!=null && System.getProperty("logback.statusListenerClass").equals("ch.qos.logback.core.status.OnConsoleStatusListener")){
			LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
			StatusPrinter.print(lc);
		}
		Scanner sc = new Scanner(System.in);
		System.out.println("Ok. Let's create a board!");
		
		Predicate<Integer> integerNotMoreThan26Predicate=new IntegerNotMoreThan26Predicate();
		Predicate<Integer> integerGreaterThanZeroPredicate=new IntegerGreaterThanZeroPredicate();
		Predicate<Integer> integerNotNegativePredicate=new IntegerNotNegativePredicate();
		
		List<Predicate<Integer>> integerNotMoreThan26AndGreaterThan0=new ArrayList<Predicate<Integer>>();
		integerNotMoreThan26AndGreaterThan0.add(integerGreaterThanZeroPredicate);
		integerNotMoreThan26AndGreaterThan0.add(integerNotMoreThan26Predicate);
		
		Board board=new BoardBuilder(sc, integerGreaterThanZeroPredicate, integerNotMoreThan26AndGreaterThan0).build();
		Pieces pieces=new PiecesBuilder(sc,integerNotNegativePredicate).build();
		sc.close();
		
		
		ChessEngineImpl chessEngine=new ChessEngineImpl(board, pieces );
		chessEngine.calculate();
		chessEngine.printArrangements();
		chessEngine.printSummary();
	}

	private void printSummary() {
		Set<Arrangement> arrangements = getArrangements();
		System.out.println("There are "+arrangements.size()+" possible arrangements. It took a time of "+java.time.Duration.between(endOfCalculations,startOfCalculations));
	}

	private void printArrangements() {
		Set<Arrangement> arrangements = getArrangements();
		System.out.println("There are "+arrangements.size()+" possible arrangements. And they are as below.");
		int i=1;
		for(Arrangement arrangement: arrangements){
			System.out.println(" "+ i++ +")  "+ arrangement);
		}
	}

	public Set<Arrangement> getArrangements() {
		return board.getArrangements();
	}

	public ChessEngineImpl(Board board, Pieces pieces) {
		super();
		this.board = board;
		this.pieces=pieces;
	}

	
	
	public void calculate(){
		startOfCalculations=Instant.now();
		new Step(board, pieces).startCalculating();
		endOfCalculations=Instant.now();
	}

	private static int getInt(Scanner sc, String questionToAsk, List<Predicate<Integer>> predicates) {
		System.out.println(questionToAsk);
		try {
			try {
				int answer = sc.nextInt();
				for(Predicate<Integer> predicate: predicates){
					if (!predicate.test(answer)) {
						throw new InvalidInputException("Invalid Input " + answer, predicate.toString());
					}
				}
				return answer;
			} catch (InputMismatchException ime) {
				sc.skip(".*");
				throw new InvalidInputException("Invalid Input ", "Not an integer or not within integer range");
			}
		} catch (InvalidInputException ime) {
			System.out.println(ime.toString());
			return getInt(sc, "Repeat question - " + questionToAsk, predicates);
		}
	}

}