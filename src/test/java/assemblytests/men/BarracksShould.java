package assemblytests.men;

import org.junit.Test;

import men.Barracks;
import men.PieceType;
import men.exceptions.NoMorePiecesException;
import men.pieces.Queen;
import men.pieces.Rook;

import static org.junit.Assert.*;

public class BarracksShould {

	@Test
	public void returnPiecesInTheSameOrderInWhichTheyWereAdded(){
		Barracks barracks=new Barracks(new Queen(), new Rook(), new Rook(), new Queen());
		assertEquals(PieceType.Queen,barracks.getAPiece().getType());
		assertEquals(PieceType.Rook,barracks.getAPiece().getType());
		assertEquals(PieceType.Rook,barracks.getAPiece().getType());
		assertEquals(PieceType.Queen,barracks.getAPiece().getType());
	}
	
	@Test(expected=NoMorePiecesException.class)
	public void throwNoMorePieceExceptionIfThereArentAnyInBarracks(){
		Barracks barracks=new Barracks(new Queen(), new Rook(), new Rook(), new Queen());
		assertEquals(PieceType.Queen,barracks.getAPiece().getType());
		assertEquals(PieceType.Rook,barracks.getAPiece().getType());
		assertEquals(PieceType.Rook,barracks.getAPiece().getType());
		assertEquals(PieceType.Queen,barracks.getAPiece().getType());
		barracks.getAPiece();
	}
	
	@Test
	public void BeAbleToPutBackAPiece(){
		Queen queen = new Queen();
		Rook rook = new Rook();
		Rook rook2 = new Rook();
		Queen queen2 = new Queen();
		Barracks barracks=new Barracks(queen, rook, rook2, queen2);
		assertEquals(PieceType.Queen,barracks.getAPiece().getType());
		assertEquals(PieceType.Rook,barracks.getAPiece().getType());
		assertEquals(PieceType.Rook,barracks.getAPiece().getType());
		assertEquals(PieceType.Queen,barracks.getAPiece().getType());
		barracks.putBackPiece(queen2);
		barracks.putBackPiece(rook2);
		barracks.putBackPiece(rook);
		barracks.putBackPiece(queen);
		assertEquals(PieceType.Queen,barracks.getAPiece().getType());
		assertEquals(PieceType.Rook,barracks.getAPiece().getType());
		assertEquals(PieceType.Rook,barracks.getAPiece().getType());
		assertEquals(PieceType.Queen,barracks.getAPiece().getType());
	}
	
}
