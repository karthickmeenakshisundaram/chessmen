package men;

import java.util.Collection;

public interface AddArrangements {

	public void add(Arrangement arrangement);
	public void addAll(Collection<Arrangement> arrangement);
	
}
