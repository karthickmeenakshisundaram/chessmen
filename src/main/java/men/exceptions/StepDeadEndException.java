package men.exceptions;

public class StepDeadEndException extends RuntimeException {

	public StepDeadEndException(RuntimeException e) {
		super(e);
	}

}
