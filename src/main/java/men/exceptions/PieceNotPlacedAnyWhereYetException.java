package men.exceptions;

import men.Piece;

public class PieceNotPlacedAnyWhereYetException extends RuntimeException {

	private Piece piece;

	
	public PieceNotPlacedAnyWhereYetException(Piece piece) {
		//TODO after piece is full of necessary methods, make a call to super(String message) constructor
		this.piece=piece;
	}

	
	
}
