package men.exceptions;

public class IllegalBoardSizeException extends RuntimeException {

	public IllegalBoardSizeException() {
		super();
		
	}

	public IllegalBoardSizeException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

	public IllegalBoardSizeException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public IllegalBoardSizeException(String message) {
		super(message);
		
	}

	public IllegalBoardSizeException(Throwable cause) {
		super(cause);
		
	}

	
	
}
