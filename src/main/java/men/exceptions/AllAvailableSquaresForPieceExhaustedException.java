package men.exceptions;

import men.Piece;

public class AllAvailableSquaresForPieceExhaustedException extends RuntimeException {

	public AllAvailableSquaresForPieceExhaustedException(Piece piece) {
		super("piece "+ piece.getType());
	}

}
