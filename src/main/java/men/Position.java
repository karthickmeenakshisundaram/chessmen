package men;

import men.exceptions.InvalidPositionException;

public class Position implements Comparable<Position>{

	private String pieceNotation;
	private String squareNotation;
	public Position(String pieceNotation, String squareNotation) throws InvalidPositionException{
		super();
		this.pieceNotation = pieceNotation;
		this.squareNotation = squareNotation;
		if(pieceNotation==null && squareNotation==null){
			throw new InvalidPositionException("Invalid Piece Notation and square notation");
		}else if(pieceNotation==null){
			throw new InvalidPositionException("Invalid Piece Notation");
		}else if(squareNotation==null){
			throw new InvalidPositionException("Invalid Square Notation");
		}
	}
	public Position(String positionString) {
		this(positionString.substring(0, 1), positionString.substring(1));
	}
	public String getPieceNotation() {
		return pieceNotation;
	}
	public String getSquareNotation() {
		return squareNotation;
	}
	@Override
	public String toString() {
		return pieceNotation + squareNotation;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pieceNotation == null) ? 0 : pieceNotation.hashCode());
		result = prime * result + ((squareNotation == null) ? 0 : squareNotation.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (pieceNotation == null) {
			if (other.pieceNotation != null)
				return false;
		} else if (!pieceNotation.equals(other.pieceNotation))
			return false;
		if (squareNotation == null) {
			if (other.squareNotation != null)
				return false;
		} else if (!squareNotation.equals(other.squareNotation))
			return false;
		return true;
	}
	@Override
	public int compareTo(Position o) {
		int squareNotationComparision = this.squareNotation.compareTo(o.getSquareNotation());
		if(squareNotationComparision==0){
			return this.pieceNotation.compareTo(o.pieceNotation);
		}else{
			return squareNotationComparision;
		}
	}
	public static Position from(String positionString) {
		return new Position(positionString);
	}
	
	
	
}
