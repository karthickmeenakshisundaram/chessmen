package men.exceptions;

public class UnSteppableException extends RuntimeException {

	public UnSteppableException(AllAvailableSquaresForPieceExhaustedException e) {
		super(e);
	}

}
