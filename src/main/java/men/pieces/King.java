package men.pieces;

import java.util.Set;

import men.Board;
import men.Piece;
import men.PieceType;
import men.Square;

public class King extends AbstractPiece implements Piece {

	@Override
	public PieceType getType() {
		return PieceType.King;
	}

	@Override
	public Set<Square> getSquaresAttacked(Board board) {
		return board.getSurroundingSquares(whereAmI());
	}

	@Override
	public int fileWeight() {
		return 0;
	}

}
