package men;

import java.util.function.Predicate;

public class IntegerNotMoreThan26Predicate implements Predicate<Integer> {

	@Override
	public boolean test(Integer t) {
		return t<=26;
	}

	@Override
	public String toString() {
		return " should not be more than 26 []";
	}
	
	

}
