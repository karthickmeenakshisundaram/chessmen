package men;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import men.exceptions.NoMorePiecesException;
import men.exceptions.NoSuchPieceWithPieceTypeFoundException;

public class Pieces implements Iterable<Piece>{

	private static final Logger log = LoggerFactory.getLogger(Pieces.class);
	private List<Piece> pieces = new LinkedList<Piece>();

	public Pieces(Collection<Piece> pieces1) {
		pieces.addAll(pieces1);
	}

	public UniquePieces getUniquePieces() throws NoMorePiecesException{
		List<Piece> uniquePiecesList=new LinkedList<Piece>();
		for(PieceType pieceType:PieceType.values()){
			try{
				uniquePiecesList.add(getPiece(pieceType));
			}catch(NoSuchPieceWithPieceTypeFoundException e){
				
			}
		}
		UniquePieces uniquePieces1=new UniquePieces(uniquePiecesList);
		return uniquePieces1;
	}
	
	private Piece getPiece(final PieceType pieceType) throws NoSuchPieceWithPieceTypeFoundException, NoMorePiecesException{
		if(pieces.size()==0){
			throw new NoMorePiecesException();
		}
		Collection<Piece> piecesOfType = CollectionUtils.<Piece>select(pieces, (p)-> p.getType().equals(pieceType));
		if(piecesOfType.size()>0){
			return piecesOfType.iterator().next();
		}else{
			throw new NoSuchPieceWithPieceTypeFoundException(pieces, pieceType);
		}
	}
	
	public boolean remove(Piece piece){
		return pieces.remove(piece);
	}
	
	public boolean add(Piece piece){
		return pieces.add(piece);
	}
	
	public int size(){
		return pieces.size();
	}
	
	protected List<Piece> getPieces(){
		return pieces;
	}

	@Override
	public Iterator<Piece> iterator() {
		return this.pieces.iterator();
	}
	
	public boolean contains(Piece piece){
		return this.pieces.contains(piece);
	}
	
	public int fileWeight(){
		int weight=0;
		for (Piece piece:pieces) {
			weight+=piece.fileWeight();
		}
		return weight;
	}
}
