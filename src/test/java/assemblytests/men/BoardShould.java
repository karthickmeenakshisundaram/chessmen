package assemblytests.men;

import static org.junit.Assert.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import men.Arrangement;
import men.Board;
import men.ChessEngineImpl;
import men.Piece;
import men.Square;
import men.Squares;
import men.exceptions.IllegalBoardSizeException;
import men.exceptions.InvalidFileException;
import men.exceptions.InvalidRowException;
import men.exceptions.InvalidSquareException;
import men.exceptions.NotSupportedException;
import men.pieces.Bishop;
import men.pieces.Knight;
import men.pieces.Queen;
import men.pieces.Rook;

public class BoardShould {

	@Test
	public void acceptPositiveMAndNValues() {
		Board board = new Board(10, 10);
	}

	@Test(expected = IllegalBoardSizeException.class)
	public void notAcceptNegativeMAndNValues1() {
		Board board = new Board(-1, 10);
	}

	@Test(expected = IllegalBoardSizeException.class)
	public void notAcceptNegativeMAndNValues2() {
		Board board = new Board(10, -1);
	}

	@Test(expected = IllegalBoardSizeException.class)
	public void notAcceptNegativeMAndNValues3() {
		Board board = new Board(-1, -1);
	}

	@Test(expected = NotSupportedException.class)
	public void notAcceptMGreaterThan26() {
		Board board = new Board(27, 1);
	}

	@Test
	public void returnBoardSpec() {
		Board board = new Board(26, 1);
		board.getSpec().equals("26 Files X 1 Row");
	}

	@Test
	public void createSquaresAsSpec() {
		Board board = new Board(8, 8);
		assertTrue("The board of 8x8 spec has not initialized squares", board.getSquares() != null);
	}

	@Test
	public void returnSameAmountSquaresAsSpec() {
		Board board = new Board(8, 8);
		assertTrue("The board of 8x8 spec is not returning 64 squares", board.getSquares().size() == 64);
	}

	@Test
	public void returnSquareByNotation() {
		Board board = new Board(8, 8);
		board.getSquare("A1").getNotation().equals("A1");
		board.getSquare("H8").getNotation().equals("H8");
	}
	
	@Test
	public void returnSquareByCoordinatesAndNotationAsExpected() {
		Board board = new Board(8, 8);
		board.getSquare(1, 1).getNotation().equals("A1");
		board.getSquare(2, 1).getNotation().equals("B1");
	}

	@Test
	public void returnSquareByCoordinates2AndNotationAsExpected() {
		Board board = new Board(26, 26);
		board.getSquare(10, 10).getNotation().equals("J10");
		board.getSquare(26, 26).getNotation().equals("Z26");
		assertTrue("10,20 didn't return 10 as it's i coordinate", board.getSquare(10, 20).getI() == 10);
		assertTrue("10,20 didn't return 20 as it's j coordinate", board.getSquare(10, 20).getJ() == 20);
		assertTrue("26,26 didn't return 26 as it's j coordinate", board.getSquare(26, 26).getJ() == 26);
	}

	@Test
	public void giveBackQueenAndRookAsPiecesOnBoardWhenQueenAndRookArePlacedOnTwoArbitrarySquares() {
		Board board = new Board(26, 26);
		Queen queenAtJ10 = new Queen();
		Rook rookAtZ26 = new Rook();
		board.getSquare(10, 10).beOccupiedBy(queenAtJ10);
		board.getSquare(26, 26).beOccupiedBy(rookAtZ26);
		Collection<Piece> pieces = board.getPiecesOnBoard();
		assert (pieces.size() == 2);
		assertTrue(pieces.contains(queenAtJ10));
		assertTrue(pieces.contains(rookAtZ26));
	}

	@Test
	public void giveBackQueenAndRookAsPiecesAtTheSameSquaresAfterQueenAndRookArePlacedOnTwoArbitrarySquares() {
		Board board = new Board(26, 26);
		Queen queenAtJ10 = new Queen();
		Rook rookAtZ26 = new Rook();
		board.getSquare(10, 10).beOccupiedBy(queenAtJ10);
		board.getSquare(26, 26).beOccupiedBy(rookAtZ26);
		assert (board.getPieceAt("J10").equals(queenAtJ10));
		assert (board.getPieceAt("Z26").equals(rookAtZ26));
	}

	@Test
	public void giveBackQueenQueenQueenAndRookRookAsPiecesAtTheSameSquaresAfterQueenQueenQueenAndRookRookArePlacedOnTwoArbitrarySquares() {
		Board board = new Board(26, 26);
		Queen queenAtA1 = new Queen();
		Queen queenAtC3 = new Queen();
		Queen queenAtF6 = new Queen();
		Rook rookAtB8 = new Rook();
		Rook rookAtH2 = new Rook();
		board.getSquare(1, 1).beOccupiedBy(queenAtA1);
		board.getSquare(3, 3).beOccupiedBy(queenAtC3);
		board.getSquare(6, 6).beOccupiedBy(queenAtF6);
		board.getSquare(2, 8).beOccupiedBy(rookAtB8);
		board.getSquare(8, 2).beOccupiedBy(rookAtH2);
		assert (board.getPieceAt("A1").equals(queenAtA1));
		assert (board.getPieceAt("C3").equals(queenAtC3));
		assert (board.getPieceAt("F6").equals(queenAtF6));
		assert (board.getPieceAt("B8").equals(rookAtB8));
		assert (board.getPieceAt("H2").equals(rookAtH2));
		assertFalse(board.getPieceAt("H2").equals(rookAtB8));
	}

	@Test
	public void sayThatItsEmptyIfNoPiecesAreOnBoard() {
		Board board = new Board(26, 26);
		assertTrue(board.isEmpty());
	}

	@Test
	public void returnSquaresInaFileIfGetSquaresInFileIsCalled() {
		Board board = new Board(26, 26);
		Set<Square> squares = board.getSquaresInFile('A');
		assertTrue(squares.size() == 26);
		for (int i = 1; i <= 26; i++) {
			assertTrue("squares doesnt contain " + "A" + i, squares.contains(new Square("A" + i)));
		}
	}
	
	@Test(expected=InvalidFileException.class)
	public void throwInvalidFileExceptionInaFileIfGetSquaresInFileIsCalledOnAFileNotPresentInTheBoard() {
		Board board = new Board(8, 8);
		Set<Square> squares = board.getSquaresInFile('Z');
	}
	
	@Test(expected=InvalidRowException.class)
	public void throwInvalidFileExceptionIfGetSquaresInInvalidRowIsCalledAgainstBoard10_20_Row21() {
		Board board = new Board(10, 20);
		Set<Square> squares = board.getSquaresInRow(21);
	}
	
	@Test(expected=InvalidSquareException.class)
	public void throwInvalidSquareExceptionIfCoordinatesDoNotExistInBoard8_8_Square_9_9() {
		Board board = new Board(8, 8);
		board.getSquare(9, 9);
	}
	
	@Test(expected=InvalidSquareException.class)
	public void throwInvalidSquareExceptionIfNotationInvalidInBoard8_8_squareZ1() {
		Board board = new Board(8, 8);
		board.getSquare("Z1");
	}

	private final Map<Integer, Character> iToA = new HashMap<Integer, Character>();
	private final Map<Character, Integer> aToI = new HashMap<Character, Integer>();
	{
		int alphabetsAscii = 65;
		for (int i = 1; i <= 26; i++) {
			iToA.put(new Integer(i), (char) alphabetsAscii);
			aToI.put((char) alphabetsAscii, new Integer(i));
			alphabetsAscii++;
		}
	}

	@Test
	public void returnSquaresInaRowIfGetSquaresInRowIsCalled() {
		Board board = new Board(26, 26);
		Set<Square> squares = board.getSquaresInRow(1);
		assertTrue(squares.size() == 26);
		for (int i = 1; i <= 26; i++) {
			assertTrue("squares doesnt contain " + iToA.get(i) + "1", squares.contains(new Square(iToA.get(i) + "1")));
		}
	}
	
	@Test
	public void returnSquaresInaFileIfGetSquaresInFileIsCalled2() {
		Board board = new Board(26, 126);
		Set<Square> squares = board.getSquaresInFile('Z');
		assertTrue(squares.size() == 126);
		for (int i = 1; i <= 126; i++) {
			assertTrue("squares doesnt contain " + "Z" + i, squares.contains(new Square("Z" + i)));
		}
	}
	
	@Test
	public void returnAllSurroundingSquaresIfGetSurroundingSquaresIsCalledBoard5_5_Square1_1() {
		Board board = new Board(5, 5);
		Set<Square> squares = board.getSurroundingSquares(new Square(1,1));
		assertEquals(3, squares.size());
		assertTrue(squares.contains(new Square("A2")));
		assertTrue(squares.contains(new Square("B1")));
		assertTrue(squares.contains(new Square("B2")));
	}
	
	@Test
	public void returnAllSurroundingSquaresIfGetSurroundingSquaresIsCalledBoard5_5_Square1_5() {
		Board board = new Board(5, 5);
		Set<Square> squares = board.getSurroundingSquares(new Square(1,5));
		assertEquals(3, squares.size());
		assertTrue(squares.contains(new Square("A4")));
		assertTrue(squares.contains(new Square("B4")));
		assertTrue(squares.contains(new Square("B5")));
	}
	
	@Test
	public void returnAllSurroundingSquaresIfGetSurroundingSquaresIsCalledBoard5_5_Square5_1() {
		Board board = new Board(5, 5);
		Set<Square> squares = board.getSurroundingSquares(new Square(5,1));
		assertEquals(3, squares.size());
		assertTrue(squares.contains(new Square("E2")));
		assertTrue(squares.contains(new Square("D2")));
		assertTrue(squares.contains(new Square("D1")));
	}
	
	@Test
	public void returnAllSurroundingSquaresIfGetSurroundingSquaresIsCalledBoard5_5_Square5_5() {
		Board board = new Board(5, 5);
		Set<Square> squares = board.getSurroundingSquares(new Square(5,5));
		assertEquals(3, squares.size());
		assertTrue(squares.contains(new Square("E4")));
		assertTrue(squares.contains(new Square("D4")));
		assertTrue(squares.contains(new Square("D5")));
	}
	
	@Test
	public void returnAllSurroundingSquaresIfGetSurroundingSquaresIsCalledBoard5_5_Square2_1() {
		Board board = new Board(5, 5);
		Set<Square> squares = board.getSurroundingSquares(new Square(2,1));
		assertEquals(5, squares.size());
		assertTrue(squares.contains(new Square("B2")));
		assertTrue(squares.contains(new Square("A1")));
		assertTrue(squares.contains(new Square("A2")));
		assertTrue(squares.contains(new Square("C1")));
		assertTrue(squares.contains(new Square("C2")));
	}
	
	@Test
	public void returnAllSurroundingSquaresIfGetSurroundingSquaresIsCalledBoard5_5_Square1_2() {
		Board board = new Board(5, 5);
		Set<Square> squares = board.getSurroundingSquares(new Square(1,2));
		assertEquals(5, squares.size());
		assertTrue(squares.contains(new Square("A3")));
		assertTrue(squares.contains(new Square("A1")));
		assertTrue(squares.contains(new Square("B2")));
		assertTrue(squares.contains(new Square("B3")));
		assertTrue(squares.contains(new Square("B1")));
	}
	
	@Test
	public void returnAllSurroundingSquaresIfGetSurroundingSquaresIsCalledBoard5_5_Square4_5() {
		Board board = new Board(5, 5);
		Set<Square> squares = board.getSurroundingSquares(new Square(4,5));
		assertEquals(5, squares.size());
		assertTrue(squares.contains(new Square("D4")));
		assertTrue(squares.contains(new Square("C4")));
		assertTrue(squares.contains(new Square("C5")));
		assertTrue(squares.contains(new Square("E4")));
		assertTrue(squares.contains(new Square("E5")));
	}
	
	@Test
	public void returnAllSurroundingSquaresIfGetSurroundingSquaresIsCalledBoard5_5_Square3_3() {
		Board board = new Board(5, 5);
		Set<Square> squares = board.getSurroundingSquares(new Square(3,3));
		assertEquals(8, squares.size());
		assertTrue(squares.contains(new Square("B4")));
		assertTrue(squares.contains(new Square("B3")));
		assertTrue(squares.contains(new Square("B2")));
		assertTrue(squares.contains(new Square("C4")));
		assertTrue(squares.contains(new Square("C2")));
		assertTrue(squares.contains(new Square("D4")));
		assertTrue(squares.contains(new Square("D3")));
		assertTrue(squares.contains(new Square("D2")));
	}
	
	@Test
	public void returnSquaresInAllDiagonalsIfGetDiagonalSquaresIsCalledBoard5_5_Square3_3() {
		Board board = new Board(5, 5);
		Set<Square> squares = board.getDiagonalSquares(new Square(3,3));
		assertEquals(8, squares.size());
		assertTrue(squares.contains(new Square("A1")));
		assertTrue(squares.contains(new Square("B2")));
		assertTrue(squares.contains(new Square("D4")));
		assertTrue(squares.contains(new Square("E5")));
		assertTrue(squares.contains(new Square("D2")));
		assertTrue(squares.contains(new Square("E1")));
		assertTrue(squares.contains(new Square("B4")));
		assertTrue(squares.contains(new Square("A5")));
	}
	
	@Test
	public void returnSquaresInAllDiagonalsIfGetDiagonalSquaresIsCalledBoard8_8_Square3_3() {
		Board board = new Board(8, 8);
		Set<Square> squares = board.getDiagonalSquares(new Square(3,3));
		assertEquals(11, squares.size());
		assertTrue(squares.contains(new Square("A1")));
		assertTrue(squares.contains(new Square("B2")));
		assertTrue(squares.contains(new Square("D4")));
		assertTrue(squares.contains(new Square("E5")));
		assertTrue(squares.contains(new Square("F6")));
		assertTrue(squares.contains(new Square("G7")));
		assertTrue(squares.contains(new Square("H8")));
		assertTrue(squares.contains(new Square("D2")));
		assertTrue(squares.contains(new Square("E1")));
		assertTrue(squares.contains(new Square("B4")));
		assertTrue(squares.contains(new Square("A5")));
	}
	
	@Test
	public void returnSquaresInAllDiagonalsIfGetDiagonalSquaresIsCalledBoard8_8_Square1_1() {
		Board board = new Board(8, 8);
		Set<Square> squares = board.getDiagonalSquares(new Square(1,1));
		assertEquals(7, squares.size());
		assertTrue(squares.contains(new Square("B2")));
		assertTrue(squares.contains(new Square("C3")));
		assertTrue(squares.contains(new Square("D4")));
		assertTrue(squares.contains(new Square("E5")));
		assertTrue(squares.contains(new Square("F6")));
		assertTrue(squares.contains(new Square("G7")));
		assertTrue(squares.contains(new Square("H8")));
	}
	@Test
	public void returnSquaresInAllDiagonalsIfGetDiagonalSquaresIsCalledBoard8_8_Square8_8() {
		Board board = new Board(8, 8);
		Set<Square> squares = board.getDiagonalSquares(new Square(8,8));
		assertEquals(7, squares.size());
		assertTrue(squares.contains(new Square("A1")));
		assertTrue(squares.contains(new Square("B2")));
		assertTrue(squares.contains(new Square("C3")));
		assertTrue(squares.contains(new Square("D4")));
		assertTrue(squares.contains(new Square("E5")));
		assertTrue(squares.contains(new Square("F6")));
		assertTrue(squares.contains(new Square("G7")));
	}
	@Test
	public void returnSquaresInAllDiagonalsIfGetDiagonalSquaresIsCalledBoard8_8_Square1_8() {
		Board board = new Board(8, 8);
		Set<Square> squares = board.getDiagonalSquares(new Square(1,8));
		assertEquals(7, squares.size());
		assertTrue(squares.contains(new Square("B7")));
		assertTrue(squares.contains(new Square("C6")));
		assertTrue(squares.contains(new Square("D5")));
		assertTrue(squares.contains(new Square("E4")));
		assertTrue(squares.contains(new Square("F3")));
		assertTrue(squares.contains(new Square("G2")));
		assertTrue(squares.contains(new Square("H1")));
	}
	@Test
	public void returnSquaresInAllDiagonalsIfGetDiagonalSquaresIsCalledBoard8_8_Square8_1() {
		Board board = new Board(8, 8);
		Set<Square> squares = board.getDiagonalSquares(new Square(8,1));
		assertEquals(7, squares.size());
		assertTrue(squares.contains(new Square("A8")));
		assertTrue(squares.contains(new Square("B7")));
		assertTrue(squares.contains(new Square("C6")));
		assertTrue(squares.contains(new Square("D5")));
		assertTrue(squares.contains(new Square("E4")));
		assertTrue(squares.contains(new Square("F3")));
		assertTrue(squares.contains(new Square("G2")));
	}
	
	@Test
	public void returnLDropSquaresIfGetLDropSquaresIsCalledBoard8_8_Square8_1() {
		Board board = new Board(8, 8);
		Set<Square> squares = board.getLDropSquares(new Square(8,1));
		assertEquals(2, squares.size());
		assertTrue("squares contained are "+squares,squares.contains(new Square("G3")));
		assertTrue("squares contained are "+squares,squares.contains(new Square("F2")));
	}
	
	@Test
	public void returnLDropSquaresIfGetLDropSquaresIsCalledBoard8_8_Square1_1() {
		Board board = new Board(8, 8);
		Set<Square> squares = board.getLDropSquares(new Square(1,1));
		assertEquals(2, squares.size());
		assertTrue("squares contained are "+squares,squares.contains(new Square("B3")));
		assertTrue("squares contained are "+squares,squares.contains(new Square("C2")));
	}
	
	@Test
	public void returnLDropSquaresIfGetLDropSquaresIsCalledBoard8_8_Square1_8() {
		Board board = new Board(8, 8);
		Set<Square> squares = board.getLDropSquares(new Square(1,8));
		assertEquals(2, squares.size());
		assertTrue("squares contained are "+squares,squares.contains(new Square("B6")));
		assertTrue("squares contained are "+squares,squares.contains(new Square("C7")));
		
	}
	
	@Test
	public void returnLDropSquaresIfGetLDropSquaresIsCalledBoard8_8_Square8_8() {
		Board board = new Board(8, 8);
		Set<Square> squares = board.getLDropSquares(new Square(8,8));
		assertEquals(2, squares.size());
		assertTrue("squares contained are "+squares,squares.contains(new Square("G6")));
		assertTrue("squares contained are "+squares,squares.contains(new Square("F7")));
	}
	
	@Test
	public void returnLDropSquaresIfGetLDropSquaresIsCalledBoard8_8_Square5_5() {
		Board board = new Board(8, 8);
		Set<Square> squares = board.getLDropSquares(new Square(5,5));//E5
		assertEquals(8, squares.size());
		assertTrue("squares contained are "+squares,squares.contains(new Square("F7")));
		assertTrue("squares contained are "+squares,squares.contains(new Square("G6")));
		assertTrue("squares contained are "+squares,squares.contains(new Square("F3")));
		assertTrue("squares contained are "+squares,squares.contains(new Square("G4")));
		assertTrue("squares contained are "+squares,squares.contains(new Square("D7")));
		assertTrue("squares contained are "+squares,squares.contains(new Square("C6")));
		assertTrue("squares contained are "+squares,squares.contains(new Square("D3")));
		assertTrue("squares contained are "+squares,squares.contains(new Square("C4")));
	}
	
	@Test
	public void returnAvailableSquaresForAPieceInABoardWhenNoPieceIsPlacedYetOnTheBoardAsTheTotalNumberOfSquaresInTheBoard(){
		
		Squares squares = new Board(8,8).getSquaresAvailable(new Queen());
		assert squares.size() == 64;
	}
	
	@Test
	public void returnAvailableSquaresForAQueenWhenAnotherQueenIsOnBoardAsTheSquaresWhichAreNotAttackedByTheAlreadyExistingQueen(){
		
		Board onBoard = new Board(8,8);
		onBoard.getSquare("A1").beOccupiedBy(new Queen());
		Squares squares = onBoard.getSquaresAvailable(new Queen());
		assertEquals(42,squares.size());
		
		for(String notation:new String[]{"A1","A2","A3","A4","A5","A6","A7","A8","B1","C1","D1","E1","F1","G1","H1","B2","C3","D4","E5","F6","G7","H8"})
			assertFalse("Squares "+squares+" contains "+notation+" which is not expected", squares.contains(new Square(notation)));
		
		for(String notation:new String[]{
					 "B3","B4","B5","B6","B7","B8",
				"C2",     "C4","C5","C6","C7","C8",
				"D2","D3",     "D5","D6","D7","D8",
				"E2","E3","E4",     "E6","E7","E8",
				"F2","F3","F4","F5",     "F7","F8",
				"G2","G3","G4","G5","G6",     "G8",
				"H2","H3","H4","H5","H6","H7"     })
			assertTrue("Squares "+squares+" do not contain "+notation+" which is unexpected", squares.contains(new Square(notation)));
	}
	
	@Test
	public void returnAvailableSquaresForAQueenWhenAnotherQueenIsOnBoardAsTheSquaresWhichAreNotAttackedByTheAlreadyExistingQueen_UsingPieceAsTheInterface(){
		
		Board onBoard = new Board(8,8);
		new Queen().place(onBoard,"A1");;
		Squares squares = onBoard.getSquaresAvailable(new Queen());
		assertEquals(42,squares.size());
		
		for(String notation:new String[]{"A1","A2","A3","A4","A5","A6","A7","A8","B1","C1","D1","E1","F1","G1","H1","B2","C3","D4","E5","F6","G7","H8"})
			assertFalse("Squares "+squares+" contains "+notation+" which is not expected", squares.contains(new Square(notation)));
		
		for(String notation:new String[]{
					 "B3","B4","B5","B6","B7","B8",
				"C2",     "C4","C5","C6","C7","C8",
				"D2","D3",     "D5","D6","D7","D8",
				"E2","E3","E4",     "E6","E7","E8",
				"F2","F3","F4","F5",     "F7","F8",
				"G2","G3","G4","G5","G6",     "G8",
				"H2","H3","H4","H5","H6","H7"     })
			assertTrue("Squares "+squares+" do not contain "+notation+" which is unexpected", squares.contains(new Square(notation)));
	}

	@Test
	public void returnAvailableSquaresForAQueenAfterTwoQueensArePlaced(){
		
		Board onBoard = new Board(8,8);
		new Queen().place(onBoard,"A1");;
		new Queen().place(onBoard,"B3");
		Squares squares = onBoard.getSquaresAvailable(new Queen());
		assertEquals(25,squares.size());
		
		for(String notation:new String[]{"A1","A2","A3","A4","A5","A6","A7","A8","B1","C1","D1","E1","F1","G1","H1","B2","C3","D4","E5","F6","G7","H8","B3","B4","B5","B6","B7","B8","D3",
				"E3","F3","G3","H3","C2", "C4","D5","E6","F7","G8"})
			assertFalse("Squares "+squares+" contains "+notation+" which is not expected", squares.contains(new Square(notation)));
		
		for(String notation:new String[]{
					 
				               "C5","C6","C7","C8",
				"D2",               "D6","D7","D8",
				"E2",     "E4",          "E7","E8",
				"F2",     "F4","F5",          "F8",
				"G2",     "G4","G5","G6",          
				"H2",     "H4","H5","H6","H7"     })
			assertTrue("Squares "+squares+" do not contain "+notation+" which is unexpected", squares.contains(new Square(notation)));
	}
	
	@Test
	public void returnAvailableSquaresForARookAfterTwoQueensArePlaced(){
		
		Board onBoard = new Board(8,8);
		new Queen().place(onBoard,"A1");;
		new Queen().place(onBoard,"B3");
		Squares squares = onBoard.getSquaresAvailable(new Rook());
		assertEquals(25,squares.size());
		
		for(String notation:new String[]{"A1","A2","A3","A4","A5","A6","A7","A8","B1","C1","D1","E1","F1","G1","H1","B2","C3","D4","E5","F6","G7","H8","B3","B4","B5","B6","B7","B8","D3",
				"E3","F3","G3","H3","C2", "C4","D5","E6","F7","G8"})
			assertFalse("Squares "+squares+" contains "+notation+" which is not expected", squares.contains(new Square(notation)));
		
		for(String notation:new String[]{
					 
				               "C5","C6","C7","C8",
				"D2",               "D6","D7","D8",
				"E2",     "E4",          "E7","E8",
				"F2",     "F4","F5",          "F8",
				"G2",     "G4","G5","G6",          
				"H2",     "H4","H5","H6","H7"     })
			assertTrue("Squares "+squares+" do not contain "+notation+" which is unexpected", squares.contains(new Square(notation)));
	}
	
	@Test
	public void returnAvailableSquaresForRookAfterABishopIsPlaced(){
		
		Board onBoard = new Board(8,8);
		new Bishop().place(onBoard,"E5");
		Squares squares = onBoard.getSquaresAvailable(new Rook());
		for(String notation:new String[]{"E5","D4","C3","B2","A1","F6","G7","H8","D6","C7","B8","F4","G3","H2","E1","E2","E3","E4","E6","E7","E8","A5","B5","C5","D5","F5","G5","H5"})
			assertFalse("Squares "+squares+" contains "+notation+" which is not expected", squares.contains(new Square(notation)));
		
		
		for(String notation:new String[]{
     "A2","A3","A4",     "A6","A7","A8",
"B1",     "B3","B4",     "B6","B7",     
"C1","C2",     "C4",     "C6",     "C8",
"D1","D2","D3",               "D7","D8",
	 
"F1","F2","F3",               "F7","F8",
"G1","G2",     "G4",     "G6",     "G8",     
"H1",     "H3","H4",     "H6","H7"     })
		assertTrue("Squares "+squares+" do not contain "+notation+" which is unexpected", squares.contains(new Square(notation)));
		assertTrue("Squares.size was "+squares.size()+" squares was "+squares.getNotations(),squares.size()==36);
	}

	@Test
	public void returnAvailableSquaresForQueenAfterABishopIsPlaced(){
		
		Board onBoard = new Board(8,8);
		new Bishop().place(onBoard,"E5");
		Squares squares = onBoard.getSquaresAvailable(new Queen());
		for(String notation:new String[]{"E5","D4","C3","B2","A1","F6","G7","H8","D6","C7","B8","F4","G3","H2","E1","E2","E3","E4","E6","E7","E8","A5","B5","C5","D5","F5","G5","H5"})
			assertFalse("Squares "+squares+" contains "+notation+" which is not expected", squares.contains(new Square(notation)));
		
		
		for(String notation:new String[]{
     "A2","A3","A4",     "A6","A7","A8",
"B1",     "B3","B4",     "B6","B7",     
"C1","C2",     "C4",     "C6",     "C8",
"D1","D2","D3",               "D7","D8",
	 
"F1","F2","F3",               "F7","F8",
"G1","G2",     "G4",     "G6",     "G8",     
"H1",     "H3","H4",     "H6","H7"     })
			assertTrue("Squares "+squares+" do not contain "+notation+" which is unexpected", squares.contains(new Square(notation)));
		assertTrue("Squares.size was "+squares.size()+" squares was "+squares.getNotations(),squares.size()==36);
		
	}

	@Test
	public void returnAvailableSquaresForKnightAfterABishopIsPlaced(){
		
		Board onBoard = new Board(8,8);
		new Bishop().place(onBoard,"E5");
		Squares squares = onBoard.getSquaresAvailable(new Knight());
		for(String notation:new String[]{"E5","D4","C3","B2","A1","F6","G7","H8","D6","C7","B8","F4","G3","H2","C4","D3","C6","D7","F3","G4","F7","G6"})
			assertFalse("Squares "+squares+" contains "+notation+" which is not expected", squares.contains(new Square(notation)));
		
		
		for(String notation:new String[]{
     "A2","A3","A4","A5", "A6","A7","A8",
"B1",     "B3","B4","B5", "B6","B7",     
"C1","C2",          "C5",           "C8",
"D1","D2",          "D5",           "D8",
"E1","E2","E3","E4",      "E6","E7","E8",	                
"F1","F2",          "F5",           "F8",
"G1","G2",          "G5",           "G8",     
"H1",     "H3","H4","H5", "H6","H7"     })
			assertTrue("Squares "+squares+" do not contain "+notation+" which is unexpected", squares.contains(new Square(notation)));
		assertTrue("Squares.size was "+squares.size()+" squares was "+squares.getNotations(),squares.size()==42);
		
	}
	
	@Test
	public void returnAvailableSquaresForBishopAfterAKnightIsPlaced(){
		
		Board onBoard = new Board(8,8);
		new Knight().place(onBoard,"E5");
		Squares squares = onBoard.getSquaresAvailable(new Bishop());
		for(String notation:new String[]{"E5","D4","C3","B2","A1","F6","G7","H8","D6","C7","B8","F4","G3","H2","C4","D3","C6","D7","F3","G4","F7","G6"})
			assertFalse("Squares "+squares+" contains "+notation+" which is not expected", squares.contains(new Square(notation)));
		
		
		for(String notation:new String[]{
     "A2","A3","A4","A5", "A6","A7","A8",
"B1",     "B3","B4","B5", "B6","B7",     
"C1","C2",          "C5",           "C8",
"D1","D2",          "D5",           "D8",
"E1","E2","E3","E4",      "E6","E7","E8",	                
"F1","F2",          "F5",           "F8",
"G1","G2",          "G5",           "G8",     
"H1",     "H3","H4","H5", "H6","H7"     })
			assertTrue("Squares "+squares+" do not contain "+notation+" which is unexpected", squares.contains(new Square(notation)));
		assertTrue("Squares.size was "+squares.size()+" squares was "+squares.getNotations(),squares.size()==42);
		
	}
	
	@Test
	public void returnAvailableSquaresForBishopAfterARookIsPlaced(){
		
		Board onBoard = new Board(8,8);
		new Rook().place(onBoard,"E5");
		Squares squares = onBoard.getSquaresAvailable(new Bishop());
		for(String notation:new String[]{"E5","D4","C3","B2","A1","F6","G7","H8","D6","C7","B8","F4","G3","H2","E1","E2","E3","E4","E6","E7","E8","A5","B5","C5","D5","F5","G5","H5"})
			assertFalse("Squares "+squares+" contains "+notation+" which is not expected", squares.contains(new Square(notation)));
		
		
		for(String notation:new String[]{
     "A2","A3","A4",     "A6","A7","A8",
"B1",     "B3","B4",     "B6","B7",     
"C1","C2",     "C4",     "C6",     "C8",
"D1","D2","D3",               "D7","D8",
	 
"F1","F2","F3",               "F7","F8",
"G1","G2",     "G4",     "G6",     "G8",     
"H1",     "H3","H4",     "H6","H7"     })
			assertTrue("Squares "+squares+" do not contain "+notation+" which is unexpected", squares.contains(new Square(notation)));
		assertTrue("Squares.size was "+squares.size()+" squares was "+squares.getNotations(),squares.size()==36);

	}
	
	@Test
	public void returnAvailableSquaresForQueenAfterARookIsPlaced(){
		
		Board onBoard = new Board(8,8);
		new Rook().place(onBoard,"E5");
		Squares squares = onBoard.getSquaresAvailable(new Queen());
		for(String notation:new String[]{"E5","D4","C3","B2","A1","F6","G7","H8","D6","C7","B8","F4","G3","H2","E1","E2","E3","E4","E6","E7","E8","A5","B5","C5","D5","F5","G5","H5"})
			assertFalse("Squares "+squares+" contains "+notation+" which is not expected", squares.contains(new Square(notation)));
		
		
		for(String notation:new String[]{
     "A2","A3","A4",     "A6","A7","A8",
"B1",     "B3","B4",     "B6","B7",     
"C1","C2",     "C4",     "C6",     "C8",
"D1","D2","D3",               "D7","D8",
	 
"F1","F2","F3",               "F7","F8",
"G1","G2",     "G4",     "G6",     "G8",     
"H1",     "H3","H4",     "H6","H7"     })
			assertTrue("Squares "+squares+" do not contain "+notation+" which is unexpected", squares.contains(new Square(notation)));
		assertTrue("Squares.size was "+squares.size()+" squares was "+squares.getNotations(),squares.size()==36);

	}
	
	@Test
	public void returnAvailableSquaresForKnightAfterARookIsPlaced(){
		
		Board onBoard = new Board(8,8);
		new Rook().place(onBoard,"E5");
		Squares squares = onBoard.getSquaresAvailable(new Knight());
		for(String notation:new String[]{"E5","E1","E2","E3","E4","E6","E7","E8","A5","B5","C5","D5","F5","G5","H5","D7","C6","F7","G6","G4","F3","D3","C4"})
			assertFalse("Squares "+squares+" contains "+notation+" which is not expected", squares.contains(new Square(notation)));
		
		
		for(String notation:new String[]{
"A1", "A2","A3","A4",     "A6","A7","A8",
"B1", "B2","B3","B4",     "B6","B7","B8",     
"C1","C2", "C3",               "C7","C8",
"D1","D2",      "D4",     "D6",     "D8",
	 
"F1","F2",      "F4",      "F6",     "F8",
"G1","G2","G3",                "G7", "G8",     
"H1","H2","H3","H4",     "H6","H7","H8" })
			assertTrue("Squares "+squares+" do not contain "+notation+" which is unexpected", squares.contains(new Square(notation)));
		assertTrue("Squares.size was "+squares.size()+" squares was "+squares.getNotations(),squares.size()==41);

	}
	
	@Test
	public void returnAvailableSquaresForRookAfterAKnightIsPlaced(){
		
		Board onBoard = new Board(8,8);
		new Knight().place(onBoard,"E5");
		Squares squares = onBoard.getSquaresAvailable(new Rook());
		for(String notation:new String[]{"E5","E1","E2","E3","E4","E6","E7","E8","A5","B5","C5","D5","F5","G5","H5","D7","C6","F7","G6","G4","F3","D3","C4"})
			assertFalse("Squares "+squares+" contains "+notation+" which is not expected", squares.contains(new Square(notation)));
		
		
		for(String notation:new String[]{
"A1", "A2","A3","A4",     "A6","A7","A8",
"B1", "B2","B3","B4",     "B6","B7","B8",     
"C1","C2", "C3",               "C7","C8",
"D1","D2",      "D4",     "D6",     "D8",
	 
"F1","F2",      "F4",      "F6",     "F8",
"G1","G2","G3",                "G7", "G8",     
"H1","H2","H3","H4",     "H6","H7","H8" })
			assertTrue("Squares "+squares+" do not contain "+notation+" which is unexpected", squares.contains(new Square(notation)));
		assertTrue("Squares.size was "+squares.size()+" squares was "+squares.getNotations(),squares.size()==41);

	}
	
	@Test
	public void returnAvailableSquaresForQueenAfterAKnightIsPlaced(){
		Board onBoard = new Board(8,8);
		new Knight().place(onBoard,"E5");
		Squares squares = onBoard.getSquaresAvailable(new Queen());
		for(String notation:new String[]{"E5","D4","C3","B2","A1","F6","G7","H8","D6","C7","B8","F4","G3","H2","E1","E2","E3","E4","E6","E7","E8","A5","B5","C5","D5","F5","G5","H5","D3","C4","D7","C6","F7","G6","F3","G4"})
			assertFalse("Squares "+squares+" contains "+notation+" which is not expected", squares.contains(new Square(notation)));
		
		
		for(String notation:new String[]{
     "A2","A3","A4",     "A6","A7","A8",
"B1",     "B3","B4",     "B6","B7",     
"C1","C2",                         "C8",
"D1","D2",                         "D8",
	 
"F1","F2",                         "F8",
"G1","G2",                         "G8",     
"H1",     "H3","H4",     "H6","H7"     })
			assertTrue("Squares "+squares+" do not contain "+notation+" which is unexpected", squares.contains(new Square(notation)));
		assertTrue("Squares.size was "+squares.size()+" squares was "+squares.getNotations(),squares.size()==28);
	}
	@Test
	public void getSquaresAvailableInFirstAvailableFile(){
		Board onBoard = new Board(4,4);
		new Knight().place(onBoard,"A1");
		Squares squares = onBoard.getSquaresAvailableInFirstAvailableFile(new Knight());
		assertEquals(6,squares.size());
		assertTrue("Square doesn't contain contain A2 ",squares.contains(onBoard.getSquare("A2")));
		assertTrue("Square doesn't contain contain A3 ",squares.contains(onBoard.getSquare("A3")));
		assertTrue("Square doesn't contain contain A4 ",squares.contains(onBoard.getSquare("A4")));
		assertTrue("Square doesn't contain contain B1 ",squares.contains(onBoard.getSquare("B1")));
		assertTrue("Square doesn't contain contain B2 ",squares.contains(onBoard.getSquare("B2")));
		assertTrue("Square doesn't contain contain B4 ",squares.contains(onBoard.getSquare("B4")));
	}
	
	
	@Test
	public void takeSnapShot() throws Exception {
		Board onBoard = new Board(8,8);
		new Knight().place(onBoard.getSquare("A1"));
		new Knight().place(onBoard.getSquare("H8"));
		new Queen().place(onBoard.getSquare("B3"));
		new Rook().place(onBoard.getSquare("C5"));
		new Bishop().place(onBoard.getSquare("D7"));
		onBoard.recordSnapShot();
		Arrangement arrangement=onBoard.getArrangements().iterator().next();
		assertEquals("HA1 QB3 RC5 BD7 HH8", arrangement.toString());
		assertEquals(5,arrangement.size());
	}
	
	

}
