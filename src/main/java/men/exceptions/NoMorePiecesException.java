package men.exceptions;

public class NoMorePiecesException extends RuntimeException {

	public NoMorePiecesException(Throwable e) {
		super(e);
	}

	public NoMorePiecesException() {
		super();
	}

}
