package men;

import java.util.Iterator;
import java.util.List;

import men.exceptions.AllAvailableSquaresForPieceExhaustedException;
import men.exceptions.UnSteppableException;
import men.exceptions.NoMorePiecesException;
import men.exceptions.StepDeadEndException;

public class Step {
	
	private Pieces pieces;
	private UniquePieces uniquePieces;

	private Board board;
	private Square onSquare;
	private Piece piece;

	private Iterator<Square> squaresAvailableForPiece;

	public Step(Board board, Pieces pieces) {
		this.board=board;
		this.pieces=pieces;
		uniquePieces=this.pieces.getUniquePieces();
	}
	
	//this is the method that would be called after creation.
	//Step should take in pieces(1), get a uniquepieces(2) object from it and get a unique piece from (3), 
		//and see what the available squares for that piece in a file(this has to be obtained from the board)
		//and then try to place it in that file. if no available square for a piece, then throw back NoSquaresForPieceException
		//If success, then take the next step, which will be handed a list of pieces, and will do the same thing..And if completely successful, then call board.getSnapShot and call addArrangements.add(Arrangement)
		//If failure, then move the current piece to the already tried pieces, put the current piece back into the pieces object that it was handed at first. 
		//And from the already available uniquePieces Object(2), take in the next unique piece and try to place this.
	public void startCalculating() throws StepDeadEndException{
		while(hasMoreUniquePieces()){
			this.piece=uniquePieces.getNext();
			/*if(pieces.size()>=2){
				squaresAvailableForPiece=board.getSquaresAvailableInFirstAvailableFile(piece).iterator();
			}else{
				squaresAvailableForPiece=board.getSquaresAvailable(piece).iterator();
			}*/
			squaresAvailableForPiece=board.getSquaresAvailableInFirstNAvailableFiles(piece,(board.getSquaresAvailable(piece).filesAvailable().length-pieces.fileWeight()+1)).iterator();
			while(hasMoreOptions()){
				this.onSquare=squaresAvailableForPiece.next();
				piece.place(onSquare);
				pieces.remove(piece);
				if(pieces.size()==0){
					board.recordSnapShot();
					piece.removeFromBoard();
					pieces.add(piece);
					continue;
				}
				new Step(board,pieces).startCalculating();
			}
			if(!hasMoreOptions()){
				piece.removeFromBoard();
				if(!pieces.contains(piece)){
					pieces.add(piece);
				}
				continue;
			}
		}
		
	}
	
	
	private boolean hasMoreOptions(){
		return squaresAvailableForPiece.hasNext();  
	}
	
	
	
	private boolean hasMoreUniquePieces(){
		return uniquePieces.hasNext();
	}

}
