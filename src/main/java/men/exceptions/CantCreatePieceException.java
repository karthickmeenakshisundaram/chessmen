package men.exceptions;

import men.Piece;

public class CantCreatePieceException extends RuntimeException {

	public CantCreatePieceException(Class<? extends Piece> class1) {
		super("Couldnt create instance of class "+class1.getSimpleName()+" . Check whether there is a no arg constructor");
	}

}
