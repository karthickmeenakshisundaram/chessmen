package assemblytests.men.pieces;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import men.Piece;
import men.PieceType;
import men.pieces.King;

public class KingShould {

	@Test
	public void returnPieceTypeAsKnight(){
		Piece king=new King();
		assertEquals(PieceType.King,king.getType());
	}
	
	@Test
	public void returnFileWeightAs0(){
		Piece king=new King();
		assertEquals(0,king.fileWeight());
	}
	
}
