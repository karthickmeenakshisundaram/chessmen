package assemblytests.men;

import static org.junit.Assert.*;

import org.junit.Test;

import men.Position;
import men.exceptions.InvalidPositionException;

public class PositionShould {

	
	@Test
	public void returnPieceNotationAndSquareNotationOnToString() {
		assertEquals("KA1",new Position("K", "A1").toString());
		assertEquals("BC1",new Position("B", "C1").toString());
		assertEquals("C1",new Position("B", "C1").getSquareNotation());
		assertEquals("B",new Position("B", "C1").getPieceNotation());
	}
	
	@Test(expected=InvalidPositionException.class)
	public void ThrowInvalidPositionExceptionIfNullPassedOnConstructors()  {
		assertEquals("KA1",new Position(null, "A1").toString());
		assertEquals("BC1",new Position("B", null).toString());
	}
}
