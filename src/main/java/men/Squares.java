package men;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections4.CollectionUtils;

import men.exceptions.InvalidFileException;
import men.exceptions.InvalidRowException;
import men.exceptions.InvalidSquareException;

public class Squares implements Iterable<Square>{

	private LinkedList<Square> squares=new LinkedList<Square>();
	
	public Squares(Collection<Square> squares1) {
		squares.addAll(squares1);
		Collections.<Square>sort(squares);
	}

	public Square getSquare(String notation) throws InvalidSquareException{
		Iterator<Square> lSquaresIterator = CollectionUtils.<Square>select(squares,(sq)->sq.getNotation().equals(notation)).iterator();
		if(lSquaresIterator.hasNext()){
			return lSquaresIterator.next();
		}else{
			throw new InvalidSquareException(notation);
		}
		
	}

	public Square getSquare(int a, int b) throws InvalidSquareException{
		Iterator<Square> lSquaresIterator = CollectionUtils.<Square>select(squares,(sq)-> sq.getI()==a && sq.getJ()==b).iterator();
		if(lSquaresIterator.hasNext()){
			return lSquaresIterator.next();
		}else{
			throw new InvalidSquareException(a,b);
		}
	}

	public List<Piece> getPiecesOnMe() {
		List<Piece> list =new LinkedList<Piece>();
		list.addAll(CollectionUtils.collect(CollectionUtils.select(squares,(sq)->sq.isOccupied()), (sq)->sq.getOccupiedBy()));
		return list;
	}

	public boolean isEmpty() {
		return CollectionUtils.select(squares,(sq)->sq.isOccupied()).size()==0;
	}

	public Set<Square> getSquaresInFile(char file) {
		HashSet<Square> hashSet = new HashSet<Square>(CollectionUtils.<Square>select(squares,(sq)-> sq.getFile()==file));
		if(hashSet.size()>0)
			return hashSet;
		else
			throw new InvalidFileException(file);
	}
	
	public Set<Square> getSquaresInRow(int row) {
		HashSet<Square> hashSet = new HashSet<Square>(CollectionUtils.<Square>select(squares,(sq)-> sq.getJ()==row));
		if(hashSet.size()>0)
			return hashSet;
		else
			throw new InvalidRowException(row);
	}

	public int size() {
		return squares.size();
	}

	public boolean contains(Square square) {
		return squares.contains(square);
	}

	public String getNotations() {
		String notation="";
		for(Square sq:squares){
			notation+=sq.getNotation()+" ";
		}
		return notation;
	}

	@Override
	public Iterator<Square> iterator() {
		return squares.iterator();
	}
	
	public boolean atleastOneSquareOccupied(){
		return getPiecesOnMe().size()>0;
	}
	
	public Character[] filesAvailable(){
		Set<Character> files=new TreeSet<Character>();
		for (Square square:squares) {
			files.add(square.getFile());
		}
		return files.toArray(new Character[files.size()]);
	}

}