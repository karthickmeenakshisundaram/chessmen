package men.exceptions;

public class InvalidInputException extends Exception {

	public InvalidInputException(String input, String message) {
		super(input+" "+message);
	}

}
