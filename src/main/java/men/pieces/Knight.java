package men.pieces;

import java.util.Set;

import men.Board;
import men.Piece;
import men.PieceType;
import men.Square;

public class Knight extends AbstractPiece implements Piece {

	@Override
	public PieceType getType() {
		return PieceType.Knight;
	}
	
	@Override
	public Set<Square> getSquaresAttacked(Board board) {
		return board.getLDropSquares(whereAmI());
	}

	@Override
	public int fileWeight() {
		return 0;
	}

}
