package men.exceptions;

import java.util.List;

import men.Piece;
import men.PieceType;

public class NoSuchPieceWithPieceTypeFoundException extends RuntimeException {

	public NoSuchPieceWithPieceTypeFoundException(List<Piece> pieces, PieceType pieceType) {
		super("PieceType expected is "+ pieceType+" .Available pieces are "+pieces);
	}

}
