package assemblytests.men.pieces;

import static org.junit.Assert.*;

import org.junit.Test;

import men.Piece;
import men.PieceType;
import men.pieces.Bishop;

public class BishopShould {

	@Test
	public void returnPieceTypeAsBishop(){
		Piece rook=new Bishop();
		assertEquals(PieceType.Bishop,rook.getType());
	}
	
	
	@Test
	public void returnFileWeightAs0(){
		Piece bishop=new Bishop();
		assertEquals(0,bishop.fileWeight());
	}
	
}
