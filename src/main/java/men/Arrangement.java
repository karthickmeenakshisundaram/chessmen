package men;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import java.util.TreeSet;

import org.apache.commons.collections4.CollectionUtils;

import men.exceptions.CantAddPositionToArrangementException;
import men.exceptions.InvalidPositionException;

public class Arrangement implements Iterable<Position>, Comparable<Arrangement>{
	
	private Set<Position> positions=new TreeSet<Position>();

	@Override
	public Iterator<Position> iterator() {
		return positions.iterator();
	}
	
	
	public void add(String pieceNotation, String squareNotation) throws CantAddPositionToArrangementException{
		try{
			positions.add(new Position(pieceNotation,squareNotation));
		}catch(InvalidPositionException ipe){
			throw new CantAddPositionToArrangementException(ipe);
		}
	}
	
	public void add(Position position){
		positions.add(position);
	}


	public int size() {
		return positions.size();
	}


	@Override
	public String toString() {
		StringJoiner stringJoiner=new StringJoiner(" ","","");
		for (Position position: positions) {
			stringJoiner.add(position.toString());
		}
		return stringJoiner.toString();
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((positions == null) ? 0 : positions.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Arrangement other = (Arrangement) obj;
		if (positions == null) {
			if (other.positions != null)
				return false;
		} else if (CollectionUtils.isEqualCollection(positions,other.positions))
			return false;
		return true;
	}


	@Override
	public int compareTo(Arrangement o) {
		if(positions.size() == o.positions.size()){
			Iterator<Position> it1=positions.iterator();
			Iterator<Position> it2=o.positions.iterator();
			while (it1.hasNext() && it2.hasNext()) {
				int compare = it1.next().compareTo(it2.next());
				if(compare==0){
					continue;
				}
				return compare;
			}
		}else if(positions.size() > o.positions.size()){
			return 1;
		}else if(positions.size() < o.positions.size()){
			return -1;
		}
		return 0;
	}
	
	public static Arrangement from(String arrangementString){
		Arrangement arrangement=new Arrangement();
		String[] positionStrings=arrangementString.split(" ");
		for (String positionString: positionStrings) {
			arrangement.add(Position.from(positionString));
		}
		return arrangement;
	}
	
	public boolean contains(Position position){
		return positions.contains(position);
	}
	
	public void make(Board onBoard){
		
	}
	
}
