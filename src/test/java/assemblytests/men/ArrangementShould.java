package assemblytests.men;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

import men.Arrangement;
import men.Position;
import men.exceptions.CantAddPositionToArrangementException;

public class ArrangementShould {

	@Test
	public void BeAbleToAddValidPositionsAndGetSizeAsSame() throws Exception {
		Arrangement arr = new Arrangement();
		arr.add("K", "A1");
		arr.add("B", "C1");
		arr.add("Q", "E1");
		arr.add("R", "F1");
		assertEquals(4,arr.size());
	}
	
	@Test(expected=CantAddPositionToArrangementException.class)
	public void ThrowCantAddPositionToArrangementExceptionIfInValidPositionsAreAdded() throws Exception {
		Arrangement arr = new Arrangement();
		arr.add(null, "A1");
	}
	
	
	@Test(expected=CantAddPositionToArrangementException.class)
	public void ThrowCantAddPositionToArrangementExceptionIfInValidPositionsAreAdded2() throws Exception {
		Arrangement arr = new Arrangement();
		arr.add("B", null);
	}
	
	@Test(expected=CantAddPositionToArrangementException.class)
	public void ThrowCantAddPositionToArrangementExceptionIfInValidPositionsAreAdded3() throws Exception {
		Arrangement arr = new Arrangement();
		arr.add(null, null);
	}
	
	@Test
	public void createOneFromArrangementString() {
		Arrangement arr = Arrangement.from("RA1");
		assertTrue(arr.contains(new Position("RA1")));
	}
	
	
	
}
