package men;

import java.util.Set;

public interface Piece extends Comparable<Piece>{

	PieceType getType();

	void setSquareIOccupy(Square square);

	void resetSquareIOccupy();

	Square whereAmI();

	Set<Square> getSquaresAttacked(Board board);
	
	void place(Square square);
	
	void place(Board board, String squareNotation);
	
	void place(Board board, int fileCoordinate, int rowCoordinate);
	
	void removeFromBoard();
	
	int fileWeight();

}
