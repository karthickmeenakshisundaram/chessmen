package unittests.men;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

import men.Piece;
import men.PieceType;
import men.Pieces;
import men.UniquePieces;
import men.exceptions.NoMorePiecesException;
import men.pieces.Bishop;
import men.pieces.Knight;
import men.pieces.Queen;
import men.pieces.Rook;

public class PiecesShould {

	@Test
	public void returnUniquePieces_onCall() {
		Pieces pieces = new Pieces(Arrays.asList(
				new Piece[] { new Queen(), new Queen(), new Rook(), new Bishop(), new Knight(), new Knight() }));
		assertEquals(4, pieces.getUniquePieces().size());
	}

	@Test
	public void returnUniquePieces_onCall2() {
		Pieces pieces = new Pieces(Arrays.asList(new Piece[] { new Queen(), new Queen(), new Rook(), new Bishop() }));
		assertEquals(3, pieces.getUniquePieces().size());
	}

	@Test(expected = NoMorePiecesException.class)
	public void returnUniquePieces_onCall3() {
		Pieces pieces = new Pieces(Arrays.asList(new Piece[] {}));
		assertEquals(0, pieces.getUniquePieces().size());
	}

	@Test
	public void returnUniquePieces_onCall4() {
		Queen queen1 = new Queen();
		Queen queen2 = new Queen();
		Rook rook = new Rook();
		Bishop bishop = new Bishop();
		Pieces pieces = new Pieces(Arrays.asList(new Piece[] { queen1, queen2, rook, bishop }));
		assertEquals(4, pieces.size());
		
		UniquePieces uniquePieces = pieces.getUniquePieces();
		assertEquals(3, uniquePieces.size());
		Piece piece = uniquePieces.getNext();
		assertTrue("pieceType is not Queen", piece.getType() == PieceType.Queen);
		assertTrue("Expected: Unique Pieces Should always be from the Pieces. This is not the case here",
				pieces.remove(piece));
		assertEquals(3, pieces.size());
		
		uniquePieces = pieces.getUniquePieces();
		assertEquals(3, uniquePieces.size());
		piece = uniquePieces.getNext();
		assertTrue("pieceType is not Queen", piece.getType() == PieceType.Queen);
		assertTrue("Expected: Unique Pieces Should always be from the Pieces. This is not the case here",
				pieces.remove(piece));
		assertEquals(2, pieces.size());
		
		uniquePieces = pieces.getUniquePieces();
		assertEquals(2, uniquePieces.size());
		piece = uniquePieces.getNext();
		assertTrue("pieceType is not Rook", piece.getType() == PieceType.Rook);
		assertTrue("Expected: Unique Pieces Should always be from the Pieces. This is not the case here",
				pieces.remove(piece));
		assertEquals(1, pieces.size());
		
		uniquePieces = pieces.getUniquePieces();
		assertEquals(1, uniquePieces.size());
		piece = uniquePieces.getNext();
		assertTrue("pieceType is not Bishop", piece.getType() == PieceType.Bishop);
		assertTrue("Expected: Unique Pieces Should always be from the Pieces. This is not the case here",
				pieces.remove(piece));
		assertEquals(0, pieces.size());
		
	}
	
	@Test
	public void returnUniquePieces_onCall5() {
		Queen queen1 = new Queen();
		Queen queen2 = new Queen();
		Queen queen3 = new Queen();
		Queen queen4 = new Queen();
		Rook rook = new Rook();
		Bishop bishop = new Bishop();
		Piece knight=new Knight();
		Pieces pieces = new Pieces(Arrays.asList(new Piece[] { queen1, queen2, queen3, queen4, rook, bishop, knight }));
		assertEquals(7, pieces.size());
		UniquePieces uniquePieces = pieces.getUniquePieces();
		assertEquals(4,uniquePieces.size());
		assertEquals(queen1, uniquePieces.getNext());
		assertEquals(knight, uniquePieces.getNext());
		assertEquals(rook, uniquePieces.getNext());
		assertEquals(bishop, uniquePieces.getNext());
	}
	
	
	@Test(expected=NoMorePiecesException.class)
	public void returnUniquePieces_6() {
		Rook rook1 = new Rook();
		Rook rook2 = new Rook();
		Pieces pieces = new Pieces(Arrays.asList(new Piece[] { rook1, rook2 }));
		assertEquals(2, pieces.size());
		UniquePieces uniquePieces = pieces.getUniquePieces();
		assertEquals(1,uniquePieces.size());
		assertEquals(rook1, uniquePieces.getNext());
		uniquePieces.getNext();
	}

}
