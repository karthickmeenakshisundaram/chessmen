package assemblytests.men;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import men.Piece;
import men.Square;
import men.exceptions.IllegalVacateException;
import men.exceptions.NotSupportedException;
import men.exceptions.PieceNotPlacedAnyWhereYetException;
import men.exceptions.SquareAlreadyOccupiedException;
import men.pieces.Queen;

public class SquareShould {

	@Test
	public void returnItsCoordinatesAfterConstructedA1() {
		Square square = new Square(1, 1);
		assert square.getNotation().equals("A1");
	}

	@Test
	public void returnItsCoordinatesAfterConstructedH8() {
		Square square = new Square(8, 8);
		assert square.getNotation().equals("H8");
	}

	@Test
	public void returnItsCoordinatesAfterConstructedE3() {
		Square square = new Square(5, 3);
		assert square.getNotation().equals("E3");
	}

	@Test
	public void returnItsCoordinatesAfterConstructedZ3() {
		Square square = new Square(26, 3);
		assert square.getNotation().equals("Z3");
	}
	@Test
	public void returnItsFileAfterConstructed26_3() {
		Square square = new Square(26, 3);
		assert square.getFile()=='Z';
	}
	@Test
	public void returnItsFileAfterConstructed1_3() {
		Square square = new Square(1, 3);
		assert square.getFile()=='A';
	}
	
	@Test
	public void returnItsFileAfterConstructedH8() {
		Square square = new Square("H8");
		assert square.getFile()=='H';
	}
	@Test
	public void returnItsFileAfterConstructedJ4() {
		Square square = new Square("J4");
		assert square.getFile()=='J';
	}

	@Test(expected = NotSupportedException.class)
	public void notSupportFileOfMoreThan26ThrowNotSupportedException27_3() {
		Square square = new Square(27, 3);
	}

	@Test
	public void returnIandJCoordinatesAsWhatWasItConstructedWith() {
		Square square = new Square(3, 4);
		assertTrue("i coordinate of 3,4 is not 3..", square.getI()==3);
		assertTrue("j coordinate of 3,4 is not 4..", square.getJ()==4);
	}
	@Test
	public void sayThatItIsNotOccupiedIfNoPieceOccupiesIt(){
		Square square = new Square(3, 4);
		assertFalse("The Square is not occupied but the square object says that it is occupied",square.isOccupied());
	}
	
	@Test
	public void sayThatItIsOccupiedIfAnyPieceOccupiesIt(){
		Square square = new Square(3, 4);
		Piece piece=new Queen();
		square.beOccupiedBy(piece);
		assertTrue("The Square has called beOccupiedBy but the square object says that it is not occupied",square.isOccupied());
	}
	
	@Test
	public void sayThatItIsOccupiedByTheSamePieceWhichWasUsedToCallBeOccupiedByOnTheSquare(){
		Square square = new Square(3, 4);
		Piece piece=new Queen();
		Piece piece2=new Queen();
		square.beOccupiedBy(piece);
		assertTrue(square.getOccupiedBy().equals(piece));
		assertFalse(square.getOccupiedBy().equals(piece2));
	}
	
	@Test(expected = SquareAlreadyOccupiedException.class)
	public void throwAnExceptionIfANewPieceTriesToOccupyItWhileAnotherPieceAlreadyOccupiesTheSquare(){
		Square square = new Square(3, 4);
		Piece piece=new Queen();
		Piece piece2=new Queen();
		square.beOccupiedBy(piece);
		square.beOccupiedBy(piece2);
	}
	
	
	
	@Test
	public void SayThatImNotOccupiedAfterASquareIsVacated(){
		Square square = new Square(3, 4);
		Piece piece=new Queen();
		Piece piece2=new Queen();
		square.beOccupiedBy(piece);
		square.vacate();
		assertFalse(square.isOccupied());
	}
	
	@Test(expected=IllegalVacateException.class)
	public void ThrowAnExceptionIfItTriesVacatingItselfWhenItIsNotOccupied(){
		Square square = new Square(3, 4);
		square.vacate();
	}
	
	@Test
	public void SayThatImOccupiedAfterItIsVacatedAndAnotherPieceOccupiesTheSquare(){
		Square square = new Square(3, 4);
		Piece piece=new Queen();
		Piece piece2=new Queen();
		square.beOccupiedBy(piece);
		square.vacate();
		square.beOccupiedBy(piece2);
		assertTrue(square.isOccupied());
	}
	
	
	@Test
	public void shouldLetAnotherPieceOccupyTheSquareAfterASquareIsVacated(){
		Square square = new Square(3, 4);
		Piece piece=new Queen();
		Piece piece2=new Queen();
		square.beOccupiedBy(piece);
		square.vacate();
		square.beOccupiedBy(piece2);
		assertTrue(square.getOccupiedBy().equals(piece2));
	}
	
	@Test(expected=PieceNotPlacedAnyWhereYetException.class)
	public void IfItIsVacated_propagateTheOccupyingSquareOfThePieceWhichOccupiedAsNull(){
		Square square = new Square(3, 4);
		Piece piece=new Queen();
		square.beOccupiedBy(piece);
		square.vacate();
		piece.whereAmI();
	}
	@Test
	public void getConstructedWithNotationAndCoordinatesCorrect(){
		Square sq=new Square("A1");
		assert sq.getI()==1;
		assert sq.getJ()==1;
		assert sq.getNotation().equals("A1");
	}
	
	@Test
	public void getConstructedWithNotationAndCoordinatesCorrect2(){
		Square sq=new Square("Z26");
		assert sq.getI()==26;
		assert sq.getJ()==26;
		assert sq.getNotation().equals("Z26");
	}
	
	@Test
	public void getConstructedWithNotationAndCoordinatesCorrect3(){
		Square sq=new Square("Z45");
		assert sq.getI()==26;
		assert sq.getJ()==45;
		assert sq.getNotation().equals("Z45");
	}
	
	@Test(expected=NotSupportedException.class)
	public void throwNotSupportedExceptionIfWrongFile(){
		Square sq=new Square("126");
	}
	@Test(expected=NotSupportedException.class)
	public void throwNotSupportedExceptionIfWrongRow(){
		Square sq=new Square("AB8");
	
	}
	
	@Test(expected=NotSupportedException.class)
	public void throwNotSupportedExceptionIfNullNotation(){
		Square sq=new Square(null);
	}
	
	@Test(expected=NotSupportedException.class)
	public void throwNotSupportedExceptionIfEmptyNotation(){
		Square sq=new Square("");
	}
	
	@Test(expected=NotSupportedException.class)
	public void throwNotSupportedExceptionIfNoRow(){
		Square sq=new Square("A");
	}
	@Test(expected=NotSupportedException.class)
	public void throwNotSupportedExceptionIfNoFile(){
		Square sq=new Square("1");
	}
	
	
	

}
