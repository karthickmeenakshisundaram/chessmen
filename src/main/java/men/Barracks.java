package men;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import men.exceptions.NoMorePiecesException;

public class Barracks {

	
	private List<Piece> pieces=new LinkedList<Piece>();

	public Barracks(Piece...pieces) {
		this(Arrays.asList(pieces));
	}

	public Barracks(List<Piece> pieces) {
		this.pieces.addAll(pieces);
	}

	public Piece getAPiece() {
		try{
			return this.pieces.remove(0);
		}catch(IndexOutOfBoundsException e){
			throw new NoMorePiecesException(e);
		}
	}
	
	public void putBackPiece(Piece piece) {
		this.pieces.add(0,piece);
	}
	
	
	

}
