package men;

import java.util.function.Predicate;

public class IntegerGreaterThanZeroPredicate implements Predicate<Integer> {

	@Override
	public boolean test(Integer t) {
		return t>0;
	}

	@Override
	public String toString() {
		return "should not be negative ";
	}

	
	
}
