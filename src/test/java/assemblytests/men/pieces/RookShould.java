package assemblytests.men.pieces;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import men.Piece;
import men.PieceType;
import men.pieces.Queen;
import men.pieces.Rook;

public class RookShould {

	@Test
	public void returnPieceTypeAsRook(){
		Piece rook=new Rook();
		assertEquals(PieceType.Rook,rook.getType());
	}
	@Test
	public void returnFileWeightAs1(){
		Piece rook=new Rook();
		assertEquals(1,rook.fileWeight());
	}
	
	
	
}
