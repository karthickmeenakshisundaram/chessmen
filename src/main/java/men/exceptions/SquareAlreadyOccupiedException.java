package men.exceptions;

import men.Square;

public class SquareAlreadyOccupiedException extends RuntimeException {

	
	private Square square;

	public SquareAlreadyOccupiedException(Square square) {
		this("square "+square.getNotation()+" is already occupied by piece "+square.getOccupiedBy());
		this.square=square;
	}

	public SquareAlreadyOccupiedException() {
		super();
		
	}

	public SquareAlreadyOccupiedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

	public SquareAlreadyOccupiedException(String message, Throwable cause) {
		super(message, cause);
		
	}

	public SquareAlreadyOccupiedException(String message) {
		super(message);
		
	}

	

	public Square getSquare() {
		return square;
	}
	
	

}
