package men.exceptions;

public class InvalidSquareException extends RuntimeException {

	public InvalidSquareException(int i, int j, String boardSpec, Throwable e ) {
		this("Invalid square coordinates ("+ i+","+j+") within board with spec"+boardSpec,e);
	}

	public InvalidSquareException(String notation, String boardSpec,Throwable e) {
		this("Invalid square notation ["+ notation+"] within board with spec"+boardSpec,e);
	}
	
	public InvalidSquareException(int i, int j) {
		this("Invalid square coordinates ("+ i+","+j+")");
	}

	public InvalidSquareException(String string) {
		super(string);
	}

	public InvalidSquareException(String message, Throwable cause) {
		super(message, cause);
	}
	
	
	
}
