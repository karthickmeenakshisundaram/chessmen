package assemblytests.men.pieces;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import men.Piece;
import men.PieceType;
import men.pieces.Knight;

public class KnightShould {

	@Test
	public void returnPieceTypeAsKnight(){
		Piece knight=new Knight();
		assertEquals(PieceType.Knight,knight.getType());
	}
	
	@Test
	public void returnFileWeightAs0(){
		Piece knight=new Knight();
		assertEquals(0,knight.fileWeight());
	}
	
}
