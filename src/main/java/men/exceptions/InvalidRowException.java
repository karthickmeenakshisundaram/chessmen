package men.exceptions;

public class InvalidRowException extends InvalidSquareException {

	public InvalidRowException(int row){
		super("Invalid row "+row);
	}
	
	public InvalidRowException(int row, String boardSpec, Throwable e){
		super("Invalid row "+row+" within board of spec "+boardSpec, e);
	}
	
}
