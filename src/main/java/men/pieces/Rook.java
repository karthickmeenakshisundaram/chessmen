package men.pieces;

import java.util.HashSet;
import java.util.Set;

import men.Board;
import men.Piece;
import men.PieceType;
import men.Square;

public class Rook extends AbstractPiece implements Piece {

	@Override
	public PieceType getType() {
		return PieceType.Rook;
	}

	@Override
	public Set<Square> getSquaresAttacked(Board board) {
		Set<Square> squares=new HashSet<Square>();
		squares.addAll(board.getSquaresInFile(whereAmI().getFile()));
		squares.addAll(board.getSquaresInRow(whereAmI().getJ()));
		return squares;
	}

	@Override
	public int fileWeight() {
		return 1;
	}

}
