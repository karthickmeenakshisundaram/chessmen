package men;

import java.util.Collection;
import java.util.Iterator;

import men.exceptions.NoMorePiecesException;

public class UniquePieces extends Pieces  {

	private Iterator<Piece> piecesIterator;
	UniquePieces(Collection<Piece> pieces1) {
		super(pieces1);
		piecesIterator = iterator();
	}

	public Piece getNext() throws NoMorePiecesException {
		if(piecesIterator.hasNext()){
			return piecesIterator.next();
		}else{
			throw new NoMorePiecesException();
		}
	}
	
	public boolean hasNext(){
		return piecesIterator.hasNext();
	}
	
}
