package assemblytests.men.pieces;

import static org.junit.Assert.*;

import org.junit.Test;

import men.Board;
import men.Piece;
import men.Square;
import men.exceptions.PieceNotPlacedAnyWhereYetException;
import men.exceptions.SquareAlreadyOccupiedException;
import men.pieces.AbstractPiece;
import men.pieces.Queen;
import men.pieces.Rook;

public class AbstractPieceShould {

	@Test
	public void knowWhatPositionItOccupies(){
		AbstractPiece abstPiece=new Queen();
		Square square=new Square(4,4);
		square.beOccupiedBy(abstPiece);
		assertTrue(abstPiece.whereAmI().equals(square));
	}
	

	@Test(expected = PieceNotPlacedAnyWhereYetException.class)
	public void sayThatThePieceHasNotBeenPlacedAnywhereIfItDoesntOccupyAnySpace(){
		AbstractPiece abstPiece=new Rook();
		abstPiece.whereAmI();
	}
	
	@Test
	public void sayIAmAtLocationOnceItHasBeenPlacedAtSquareInABoard(){
		Board board = new Board(8,8);
		Piece piece=new Rook();
		piece.place(board, "A1");
		assertEquals(piece.whereAmI(),board.getSquare("A1"));
		piece.place(board, "B1");
		assertEquals(piece.whereAmI(),board.getSquare("B1"));
	}
	
	@Test
	public void sayIAmAtLocationOnceItHasBeenPlacedAtSquareInABoard_usingCoordinates(){
		Board board = new Board(8,8);
		Piece piece=new Rook();
		piece.place(board, 1,1);
		assertEquals(piece.whereAmI(),board.getSquare("A1"));
		piece.place(board, 2,1);
		assertEquals(piece.whereAmI(),board.getSquare("B1"));
	}
	
	@Test
	public void sayIAmAtLocationOnceItHasBeenPlacedAtSquareInABoard_usingSquare(){
		Board board = new Board(8,8);
		Piece piece=new Rook();
		piece.place(board.getSquare(1, 1));
		assertEquals(piece.whereAmI(),board.getSquare("A1"));
		assertEquals(piece,board.getSquare(1, 1).getOccupiedBy());
		piece.place(board.getSquare(2, 1));
		assertEquals(piece.whereAmI(),board.getSquare("B1"));
		assertTrue(board.getSquare(2, 1).isOccupied());
		assertEquals(piece,board.getSquare(2, 1).getOccupiedBy());
		
		assertNotEquals(piece,board.getSquare(1, 1).getOccupiedBy());
		assertFalse(board.getSquare(1, 1).isOccupied());
	}
	
	@Test(expected=SquareAlreadyOccupiedException.class)
	public void sayAnotherPieceCannotBePlacedOnASquareWhereThereIsAPieceAlready(){
		Board board = new Board(8,8);
		Piece piece=new Rook();
		piece.place(board, "A1");
		assertEquals(piece.whereAmI(),board.getSquare("A1"));
		Piece piece2=new Queen();
		piece2.place(board, "A1");
	}
	
}
