package men;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.experimental.max.MaxCore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import men.exceptions.IllegalBoardSizeException;
import men.exceptions.InvalidFileException;
import men.exceptions.InvalidRowException;
import men.exceptions.InvalidSquareException;
import men.exceptions.NotSupportedException;

public class Board {

	private static final Logger log = LoggerFactory.getLogger(Board.class);
	
	private int maxRows;
	private int maxFiles;
	private Squares squares;
	private Set<Arrangement> arrangements=new TreeSet<Arrangement>();
	
	public Board(int m, int n) {
		if(m<1 || n< 1){
			throw new IllegalBoardSizeException("Illegal Board Spec. ["+m+"X"+n+"] Both m and n will have to be positive");
		}
		if(m>26){
			throw new NotSupportedException("An m value of greater than 26 is not supported");
		}
		this.maxFiles=m;
		this.maxRows=n;
		Set<Square> setOfsquares=new TreeSet<Square>();
		for (int i = 0; i < maxFiles; i++) {
			for (int j = 0; j < maxRows; j++) {
				setOfsquares.add(new Square(i+1, j+1));
			}
		}
		this.squares=new Squares(setOfsquares);
	}

	public String getSpec() {
		return maxFiles+" Files X "+maxRows+" Row";
	}

	public Squares getSquares() {
		return squares;
	}

	public Square getSquare(String notation) throws InvalidSquareException{
		try{
			return squares.getSquare(notation);
		}catch(InvalidSquareException e){
			throw new InvalidSquareException(notation, this.getSpec(),e);
		}
	}
	
	public Square getSquare(int a, int b) throws InvalidSquareException{
		try {
			return squares.getSquare(a,b);
		} catch (InvalidSquareException e) {
			throw new InvalidSquareException(a,b, this.getSpec(),e);
		}
	}
	
	public List<Piece> getPiecesOnBoard() {
		return squares.getPiecesOnMe();
	}

	public Piece getPieceAt(String notation) {
		return getSquare(notation).getOccupiedBy();
	}

	public boolean isEmpty() {
		return squares.isEmpty();
	}

	public Set<Square> getSquaresInFile(char file) throws InvalidFileException{
		try {
			return squares.getSquaresInFile(file);
		} catch (InvalidFileException e) {
			throw new InvalidFileException(file, this.getSpec(),e);
		}
	}

	public Set<Square> getSquaresInRow(int row) throws InvalidRowException{
		try {
			return squares.getSquaresInRow(row);
		} catch (InvalidRowException e) {
			throw new InvalidRowException(row, this.getSpec(),e);
		}
	}

	public Set<Square> getSurroundingSquares(Square square) {
		Set<Square> surroundingSquares=new HashSet<Square>();
		int i=square.getI();//3 / C
		int j=square.getJ();//3
		if(i+1<=maxFiles){
			surroundingSquares.add(getSquare(i+1,j));
			if(j+1<=maxRows){
				surroundingSquares.add(getSquare(i+1,j+1));
			}
			if(j-1>0){
				surroundingSquares.add(getSquare(i+1,j-1));
			}
		}
		if(i-1>0){
			surroundingSquares.add(getSquare(i-1,j));
			if(j+1<=maxRows){
				surroundingSquares.add(getSquare(i-1,j+1));
			}
			if(j-1>0){
				surroundingSquares.add(getSquare(i-1,j-1));
			}
		}
		if(j+1<=maxRows){
			surroundingSquares.add(getSquare(i,j+1));
		}
		if(j-1>0){
			surroundingSquares.add(getSquare(i,j-1));
		}
		return surroundingSquares;
	}
	
	public Set<Square> getDiagonalSquares(Square square) {
		Set<Square> diagonalSquares=new HashSet<Square>();
		int i=square.getI();//3 / C
		int j=square.getJ();//3
		
		
		for(int a=1;i-a >=1 && j-a>=1  ;a++){
			diagonalSquares.add(getSquare(i-a,j-a));
		}
		for(int a=1;i+a <=maxFiles && j+a<=maxRows  ;a++){
			diagonalSquares.add(getSquare(i+a,j+a));
		}
		for(int a=1;i+a <=maxFiles && j-a>=1  ;a++){
			diagonalSquares.add(getSquare(i+a,j-a));
		}
		for(int a=1;i-a >=1 && j+a<=maxRows  ;a++){
			diagonalSquares.add(getSquare(i-a,j+a));
		}
		
		return diagonalSquares;
	}

	public Set<Square> getLDropSquares(Square square) {
		Set<Square> lDropSquares=new HashSet<Square>();
		int i=square.getI();//3 / C
		int j=square.getJ();//3
		
		if((i-1) >0 && (j-2) >0){
			lDropSquares.add(getSquare(i-1, j-2));
		}
		if((i-2) >0 && (j-1) >0){
			lDropSquares.add(getSquare(i-2, j-1));
		}
		if((i+1) <=maxFiles && (j+2) <=maxRows){
			lDropSquares.add(getSquare(i+1, j+2));
		}
		if((i+2) <=maxFiles && (j+1) <=maxRows){
			lDropSquares.add(getSquare(i+2, j+1));
		}
		if((i+1) <=maxFiles && (j-2) >0){
			lDropSquares.add(getSquare(i+1, j-2));
		}
		if((i-1) >0 && (j+2) <=maxRows){
			lDropSquares.add(getSquare(i-1, j+2));
		}
		if((i+2) <=maxFiles && (j-1) >0){
			lDropSquares.add(getSquare(i+2, j-1));
		}
		if((i-2) >0 && (j+1) <=maxRows){
			lDropSquares.add(getSquare(i-2, j+1));
		}
		
		//M1,N2, M2,N1
		//M-1,N2,  M-2,N1
		//M1, N-2,  M2, N-1
		//M-1,N-2   M-2,N-1
		
		return lDropSquares;
	}
	
	public Squares getSquaresAvailable(Piece forPiece) {
		if (isEmpty())
			return getSquares();
		else{
			Set<Square> squaresAvailable=new HashSet<Square>();
			Set<Square> squaresUnAvailable=new HashSet<Square>();
			Collection<Piece> pieces = getPiecesOnBoard();
			for(Piece piece: pieces){
				squaresUnAvailable.addAll(piece.getSquaresAttacked(this));
				squaresUnAvailable.add(piece.whereAmI());
				switch(forPiece.getType()){
				case Bishop:
					squaresUnAvailable.addAll(getDiagonalSquares(piece.whereAmI()));
					break;
				case Rook:
					squaresUnAvailable.addAll(getSquaresInFile(piece.whereAmI().getFile()));
					squaresUnAvailable.addAll(getSquaresInRow(piece.whereAmI().getJ()));
					break;
				case Queen:
					squaresUnAvailable.addAll(getSquaresInFile(piece.whereAmI().getFile()));
					squaresUnAvailable.addAll(getSquaresInRow(piece.whereAmI().getJ()));
					squaresUnAvailable.addAll(getDiagonalSquares(piece.whereAmI()));
					break;
				case Knight:
					squaresUnAvailable.addAll(getLDropSquares(piece.whereAmI()));
					break;
				}
			}
			
			squaresAvailable.addAll(CollectionUtils.subtract(getSquares(), squaresUnAvailable));
			return new Squares(squaresAvailable);
		}
	}
	
	public Squares getSquaresAvailableInFirstAvailableFile(Piece forPiece) {
		return getSquaresAvailableInFirstNAvailableFiles(forPiece, 1);
	}
	
	public Squares getSquaresAvailableInFirstNAvailableFiles(Piece forPiece, int noOfFilesToBeAdded) {
		Set<Square> squaresAvailableSubSetAsASet=new TreeSet<Square>();
		Squares squaresAvailable=getSquaresAvailable(forPiece);
		int noOfFilesAdded=0;
		for(int fileNumber=1; (fileNumber<=maxFiles)&&(noOfFilesAdded<noOfFilesToBeAdded) ;fileNumber++){
			try{
			Set<Square> squares2 = squaresAvailable.getSquaresInFile(Square.getFileForI(fileNumber));
			if(squares2.size()>0){
				noOfFilesAdded++;
				squaresAvailableSubSetAsASet.addAll(squares2);
				if((noOfFilesAdded==1) && new Squares(this.getSquaresInFile(Square.getFileForI(fileNumber))).atleastOneSquareOccupied()){
					squaresAvailableSubSetAsASet.addAll(squaresAvailable.getSquaresInFile(Square.getFileForI(++fileNumber)));
				}
			
			}
			}catch(InvalidFileException ivfe){
				//do nothing
			}
		}
		return new Squares(squaresAvailableSubSetAsASet);
		
	}

	public void recordSnapShot() {
		Arrangement arrangement=new Arrangement();
		List<Piece> piecesOnBoard = getPiecesOnBoard();
		Collections.<Piece>sort(piecesOnBoard,(p1,p2)->p1.whereAmI().getNotation().compareTo(p2.whereAmI().getNotation()));
		for (Piece piece:piecesOnBoard) {
			arrangement.add(piece.getType().getNotation(),piece.whereAmI().getNotation());
		}
		log.debug("Arrangement is "+ arrangement);

		
		arrangements.add(arrangement);
	}

	public Set<Arrangement> getArrangements() {
		return arrangements;
	}

	
	
	/*public int getMaxColumn() {
		return maxColumn;
	}
	*/	
	public int getMaxRows() {
		return maxRows;
	}

	

	
	

	
	
	
}
