package men.exceptions;

public class InvalidFileException extends InvalidSquareException {

	public InvalidFileException(char file) {
		super("Invalid File "+file);
	}
	
	public InvalidFileException(char file, String boardSpec, Throwable e){
		super("Invalid file "+file+" within board of spec "+boardSpec,e);
	}

}
