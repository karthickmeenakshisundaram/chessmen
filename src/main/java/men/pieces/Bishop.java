package men.pieces;

import java.util.Set;

import men.Board;
import men.Piece;
import men.PieceType;
import men.Square;

public class Bishop extends AbstractPiece implements Piece {

	@Override
	public PieceType getType() {
		return PieceType.Bishop;
	}

	@Override
	public Set<Square> getSquaresAttacked(Board board) {
		return board.getDiagonalSquares(whereAmI());
	}

	@Override
	public int fileWeight() {
		return 0;
	}

}
