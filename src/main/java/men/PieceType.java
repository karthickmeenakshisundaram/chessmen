package men;

public enum PieceType {
	
	Queen("Q"), Knight("H"), Rook("R"), Bishop("B"), King("K");
	String notation;
	
	private PieceType(String notation){
		this.notation=notation;
	}

	public String getNotation() {
		return notation;
	}
	
	
	

	
	
}
